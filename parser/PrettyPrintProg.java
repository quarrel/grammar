import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class PrettyPrintProg {
    static Integer lineLength = 80;

    public static void main(String[] args) throws Exception {
        String inputFile = null;
        if (args.length > 0)
            inputFile = args[0];
        InputStream is = System.in;
        if (inputFile != null)
            is = new FileInputStream(inputFile);
        CharStream input = CharStreams.fromStream(is);
        QuarrelLexer lexer = new QuarrelLexer(input);
        CommonTokenStream tokens = new CommonTokenStream((TokenSource) lexer);
        QuarrelParser parser = new QuarrelParser(tokens);
        ParseTree root = parser.program();
        System.out.println(printSyntaxTree(parser, root));
    } 

    public static String printSyntaxTree(Parser parser, ParseTree root) {
        StringBuilder buf = new StringBuilder();
        recursive(root, buf, 0, Arrays.asList(parser.getRuleNames()));
        return buf.toString();
    }
    
    private static void recursive(ParseTree aRoot, StringBuilder buf, int offset, List<String> ruleNames) {
        StringBuilder lineBuf = new StringBuilder();
        String oneLiner = Trees.toStringTree(aRoot, ruleNames);

        for (int i = 0; i < offset; i++) {
            lineBuf.append("  ");
        }
        
        if (oneLiner.length() + lineBuf.length() < lineLength) {
            buf.append(lineBuf).append(oneLiner).append(")").append("\n");
        } else {
            lineBuf.append("(").append(Trees.getNodeText(aRoot, ruleNames));
            if (aRoot instanceof ParserRuleContext) {
                ParserRuleContext prc = (ParserRuleContext) aRoot;
                if (prc.children != null) {
                    for (ParseTree child : prc.children) {
                        String childOneLiner = Trees.toStringTree(child, ruleNames);
                        if (childOneLiner.toCharArray()[0] == ' ') childOneLiner = "\\s";
                        if (lineBuf.length() + childOneLiner.length() < lineLength) {
                            buf.append(lineBuf).append(" ").append(childOneLiner).append(")").append("\n");
                        } else {
                            recursive(child, buf.append(lineBuf).append("\n"), offset + 1, ruleNames);
                        }
                    }
                }
            }
        }
        
    }
}
