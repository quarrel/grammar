import org.antlr.v4.runtime.*;

/**
 * All parser methods that used in grammar (p, prev, notLineTerminator, etc.)
 * should start with lower case char similar to parser rules.
 */
public abstract class QuarrelParserBase extends Parser
{
    public QuarrelParserBase(TokenStream input) {
        super(input);
    }

    /**
     * Whether the previous token value equals to @param str
     */
    protected boolean prev(String str) {
        return _input.LT(-1).getText().equals(str);
    }

    /**
     * Whether the next token value equals to @param str
     */
//    protected boolean notRoutine() {
//        System.out.print("Scanning next token...");
//        int scan = 1;
//        while (_input.LT(scan++).getType() != (QuarrelParser.Space));
//        System.out.print("token: ");
//        System.out.println(_input.LT(scan).getText());
//        System.out.print(_input.LT(scan).getText());
//        System.out.print(" == ");
//        System.out.println(token);
//        boolean result = _input.LT(scan).getText().trim().equals(token);
//        System.out.print("Is it '=>'? ");
//        System.out.println(result);
//        return !result;
//    }

    protected boolean notOpenBrace() {
        int nextTokenType = _input.LT(1).getType();
        return nextTokenType != QuarrelParser.OpenBrace;
    }

    protected boolean notOpenParen() {
        int nextTokenType = _input.LT(1).getType();
        return nextTokenType != QuarrelParser.OpenParen;
    }

    protected boolean notAfterContainer() {
        int prevTokenType = _input.LT(-1).getType();
        return prevTokenType != QuarrelParser.Container;
    }

    protected boolean closeBrace() {
        return _input.LT(1).getType() == QuarrelParser.CloseBrace;
    }
    
    
    protected boolean closeParen() { return _input.LT(1).getType() == QuarrelParser.CloseParen; }


    /**
     * Returns {@code true} iff on the current index of the parser's
     * token stream a token exists on the {@code HIDDEN} channel which
     * either is a line terminator, or is a multi line comment that
     * contains a line terminator.
     *
     * @return {@code true} iff on the current index of the parser's
     * token stream a token exists on the {@code HIDDEN} channel which
     * either is a line terminator, or is a multi line comment that
     * contains a line terminator.
     */
    protected boolean lineTerminatorAhead() {

        int cursor = 1;
        // Get the token ahead of the current index.
        int possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - cursor;
        Token ahead = _input.get(possibleIndexEosToken);

        if (ahead.getType() == QuarrelParser.LineTerminator || ahead.getType() == QuarrelParser.SingleLineComment) {
            // There is definitely a line terminator ahead.
            return true;
        }

        while (ahead.getType() == QuarrelParser.Space || ahead.getType() == QuarrelParser.BracketedComment) {
            // Get the token ahead of the current spaces and bracketed comments
            possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - ++cursor;
            ahead = _input.get(possibleIndexEosToken);
        }

        // Get the token's text and type.
        String text = ahead.getText();
        int type = ahead.getType();

        // Check if the token is, or contains a line terminator.
        return (type == QuarrelParser.MultiLineComment && (text.contains("\r") || text.contains("\n")))
                || (type == QuarrelParser.LineTerminator);
    }

}