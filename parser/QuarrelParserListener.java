// Generated from /home/jake/git/quarrel-grammar/grammar/QuarrelParser.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link QuarrelParser}.
 */
public interface QuarrelParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(QuarrelParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(QuarrelParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#sourceElements}.
	 * @param ctx the parse tree
	 */
	void enterSourceElements(QuarrelParser.SourceElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#sourceElements}.
	 * @param ctx the parse tree
	 */
	void exitSourceElements(QuarrelParser.SourceElementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#sourceElement}.
	 * @param ctx the parse tree
	 */
	void enterSourceElement(QuarrelParser.SourceElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#sourceElement}.
	 * @param ctx the parse tree
	 */
	void exitSourceElement(QuarrelParser.SourceElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void enterExpressionStatement(QuarrelParser.ExpressionStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void exitExpressionStatement(QuarrelParser.ExpressionStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#compoundLiteral}.
	 * @param ctx the parse tree
	 */
	void enterCompoundLiteral(QuarrelParser.CompoundLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#compoundLiteral}.
	 * @param ctx the parse tree
	 */
	void exitCompoundLiteral(QuarrelParser.CompoundLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#sequenceElements}.
	 * @param ctx the parse tree
	 */
	void enterSequenceElements(QuarrelParser.SequenceElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#sequenceElements}.
	 * @param ctx the parse tree
	 */
	void exitSequenceElements(QuarrelParser.SequenceElementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#listElements}.
	 * @param ctx the parse tree
	 */
	void enterListElements(QuarrelParser.ListElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#listElements}.
	 * @param ctx the parse tree
	 */
	void exitListElements(QuarrelParser.ListElementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#listElement}.
	 * @param ctx the parse tree
	 */
	void enterListElement(QuarrelParser.ListElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#listElement}.
	 * @param ctx the parse tree
	 */
	void exitListElement(QuarrelParser.ListElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#recordElements}.
	 * @param ctx the parse tree
	 */
	void enterRecordElements(QuarrelParser.RecordElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#recordElements}.
	 * @param ctx the parse tree
	 */
	void exitRecordElements(QuarrelParser.RecordElementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#patternLiteral}.
	 * @param ctx the parse tree
	 */
	void enterPatternLiteral(QuarrelParser.PatternLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#patternLiteral}.
	 * @param ctx the parse tree
	 */
	void exitPatternLiteral(QuarrelParser.PatternLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#patternElements}.
	 * @param ctx the parse tree
	 */
	void enterPatternElements(QuarrelParser.PatternElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#patternElements}.
	 * @param ctx the parse tree
	 */
	void exitPatternElements(QuarrelParser.PatternElementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(QuarrelParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(QuarrelParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#fieldOperator}.
	 * @param ctx the parse tree
	 */
	void enterFieldOperator(QuarrelParser.FieldOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#fieldOperator}.
	 * @param ctx the parse tree
	 */
	void exitFieldOperator(QuarrelParser.FieldOperatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code PriorContainerFieldAssignment}
	 * labeled alternative in {@link QuarrelParser#fieldAssignment}.
	 * @param ctx the parse tree
	 */
	void enterPriorContainerFieldAssignment(QuarrelParser.PriorContainerFieldAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code PriorContainerFieldAssignment}
	 * labeled alternative in {@link QuarrelParser#fieldAssignment}.
	 * @param ctx the parse tree
	 */
	void exitPriorContainerFieldAssignment(QuarrelParser.PriorContainerFieldAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FieldExpressionAssignment}
	 * labeled alternative in {@link QuarrelParser#fieldAssignment}.
	 * @param ctx the parse tree
	 */
	void enterFieldExpressionAssignment(QuarrelParser.FieldExpressionAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FieldExpressionAssignment}
	 * labeled alternative in {@link QuarrelParser#fieldAssignment}.
	 * @param ctx the parse tree
	 */
	void exitFieldExpressionAssignment(QuarrelParser.FieldExpressionAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComputedFieldExpressionAssignment}
	 * labeled alternative in {@link QuarrelParser#fieldAssignment}.
	 * @param ctx the parse tree
	 */
	void enterComputedFieldExpressionAssignment(QuarrelParser.ComputedFieldExpressionAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComputedFieldExpressionAssignment}
	 * labeled alternative in {@link QuarrelParser#fieldAssignment}.
	 * @param ctx the parse tree
	 */
	void exitComputedFieldExpressionAssignment(QuarrelParser.ComputedFieldExpressionAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RoutineField}
	 * labeled alternative in {@link QuarrelParser#fieldAssignment}.
	 * @param ctx the parse tree
	 */
	void enterRoutineField(QuarrelParser.RoutineFieldContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RoutineField}
	 * labeled alternative in {@link QuarrelParser#fieldAssignment}.
	 * @param ctx the parse tree
	 */
	void exitRoutineField(QuarrelParser.RoutineFieldContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#fieldName}.
	 * @param ctx the parse tree
	 */
	void enterFieldName(QuarrelParser.FieldNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#fieldName}.
	 * @param ctx the parse tree
	 */
	void exitFieldName(QuarrelParser.FieldNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#invocationExpression}.
	 * @param ctx the parse tree
	 */
	void enterInvocationExpression(QuarrelParser.InvocationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#invocationExpression}.
	 * @param ctx the parse tree
	 */
	void exitInvocationExpression(QuarrelParser.InvocationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#invocable}.
	 * @param ctx the parse tree
	 */
	void enterInvocable(QuarrelParser.InvocableContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#invocable}.
	 * @param ctx the parse tree
	 */
	void exitInvocable(QuarrelParser.InvocableContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#argumentElements}.
	 * @param ctx the parse tree
	 */
	void enterArgumentElements(QuarrelParser.ArgumentElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#argumentElements}.
	 * @param ctx the parse tree
	 */
	void exitArgumentElements(QuarrelParser.ArgumentElementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(QuarrelParser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(QuarrelParser.ArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LiteralExpression}
	 * labeled alternative in {@link QuarrelParser#nonBinaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterLiteralExpression(QuarrelParser.LiteralExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LiteralExpression}
	 * labeled alternative in {@link QuarrelParser#nonBinaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitLiteralExpression(QuarrelParser.LiteralExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SpecArgExpression}
	 * labeled alternative in {@link QuarrelParser#nonBinaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterSpecArgExpression(QuarrelParser.SpecArgExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SpecArgExpression}
	 * labeled alternative in {@link QuarrelParser#nonBinaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitSpecArgExpression(QuarrelParser.SpecArgExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code InvokeExpression}
	 * labeled alternative in {@link QuarrelParser#nonBinaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterInvokeExpression(QuarrelParser.InvokeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code InvokeExpression}
	 * labeled alternative in {@link QuarrelParser#nonBinaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitInvokeExpression(QuarrelParser.InvokeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ContainerExpression}
	 * labeled alternative in {@link QuarrelParser#nonBinaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterContainerExpression(QuarrelParser.ContainerExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ContainerExpression}
	 * labeled alternative in {@link QuarrelParser#nonBinaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitContainerExpression(QuarrelParser.ContainerExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CompoundExpression}
	 * labeled alternative in {@link QuarrelParser#nonBinaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompoundExpression(QuarrelParser.CompoundExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CompoundExpression}
	 * labeled alternative in {@link QuarrelParser#nonBinaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompoundExpression(QuarrelParser.CompoundExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BaseExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterBaseExpression(QuarrelParser.BaseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BaseExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitBaseExpression(QuarrelParser.BaseExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AdditiveExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpression(QuarrelParser.AdditiveExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AdditiveExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpression(QuarrelParser.AdditiveExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RelationalExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterRelationalExpression(QuarrelParser.RelationalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RelationalExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitRelationalExpression(QuarrelParser.RelationalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalAndExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterLogicalAndExpression(QuarrelParser.LogicalAndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalAndExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitLogicalAndExpression(QuarrelParser.LogicalAndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code UnaryPostfixExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryPostfixExpression(QuarrelParser.UnaryPostfixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code UnaryPostfixExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryPostfixExpression(QuarrelParser.UnaryPostfixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExponentiationExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterExponentiationExpression(QuarrelParser.ExponentiationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExponentiationExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitExponentiationExpression(QuarrelParser.ExponentiationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalOrExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOrExpression(QuarrelParser.LogicalOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalOrExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOrExpression(QuarrelParser.LogicalOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code InExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterInExpression(QuarrelParser.InExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code InExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitInExpression(QuarrelParser.InExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code TransformRightExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterTransformRightExpression(QuarrelParser.TransformRightExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code TransformRightExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitTransformRightExpression(QuarrelParser.TransformRightExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code TransformLeftExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterTransformLeftExpression(QuarrelParser.TransformLeftExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code TransformLeftExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitTransformLeftExpression(QuarrelParser.TransformLeftExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MemberBSlashExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterMemberBSlashExpression(QuarrelParser.MemberBSlashExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MemberBSlashExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitMemberBSlashExpression(QuarrelParser.MemberBSlashExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BitAndExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterBitAndExpression(QuarrelParser.BitAndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BitAndExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitBitAndExpression(QuarrelParser.BitAndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RoutineExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterRoutineExpression(QuarrelParser.RoutineExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RoutineExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitRoutineExpression(QuarrelParser.RoutineExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RecordAssignmentOperatorExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterRecordAssignmentOperatorExpression(QuarrelParser.RecordAssignmentOperatorExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RecordAssignmentOperatorExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitRecordAssignmentOperatorExpression(QuarrelParser.RecordAssignmentOperatorExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BitOrExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterBitOrExpression(QuarrelParser.BitOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BitOrExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitBitOrExpression(QuarrelParser.BitOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalXorExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterLogicalXorExpression(QuarrelParser.LogicalXorExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalXorExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitLogicalXorExpression(QuarrelParser.LogicalXorExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code UnaryPrefixExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryPrefixExpression(QuarrelParser.UnaryPrefixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code UnaryPrefixExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryPrefixExpression(QuarrelParser.UnaryPrefixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AssignmentOperatorExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentOperatorExpression(QuarrelParser.AssignmentOperatorExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AssignmentOperatorExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentOperatorExpression(QuarrelParser.AssignmentOperatorExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BitXOrExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterBitXOrExpression(QuarrelParser.BitXOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BitXOrExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitBitXOrExpression(QuarrelParser.BitXOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code EqualityExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterEqualityExpression(QuarrelParser.EqualityExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code EqualityExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitEqualityExpression(QuarrelParser.EqualityExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ThenExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterThenExpression(QuarrelParser.ThenExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ThenExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitThenExpression(QuarrelParser.ThenExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MultiplicativeExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicativeExpression(QuarrelParser.MultiplicativeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MultiplicativeExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicativeExpression(QuarrelParser.MultiplicativeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BitShiftExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterBitShiftExpression(QuarrelParser.BitShiftExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BitShiftExpression}
	 * labeled alternative in {@link QuarrelParser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitBitShiftExpression(QuarrelParser.BitShiftExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#leftTransform}.
	 * @param ctx the parse tree
	 */
	void enterLeftTransform(QuarrelParser.LeftTransformContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#leftTransform}.
	 * @param ctx the parse tree
	 */
	void exitLeftTransform(QuarrelParser.LeftTransformContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#rightTransform}.
	 * @param ctx the parse tree
	 */
	void enterRightTransform(QuarrelParser.RightTransformContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#rightTransform}.
	 * @param ctx the parse tree
	 */
	void exitRightTransform(QuarrelParser.RightTransformContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#formalRoutineExpression}.
	 * @param ctx the parse tree
	 */
	void enterFormalRoutineExpression(QuarrelParser.FormalRoutineExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#formalRoutineExpression}.
	 * @param ctx the parse tree
	 */
	void exitFormalRoutineExpression(QuarrelParser.FormalRoutineExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#routinePattern}.
	 * @param ctx the parse tree
	 */
	void enterRoutinePattern(QuarrelParser.RoutinePatternContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#routinePattern}.
	 * @param ctx the parse tree
	 */
	void exitRoutinePattern(QuarrelParser.RoutinePatternContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#routineBody}.
	 * @param ctx the parse tree
	 */
	void enterRoutineBody(QuarrelParser.RoutineBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#routineBody}.
	 * @param ctx the parse tree
	 */
	void exitRoutineBody(QuarrelParser.RoutineBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentOperator(QuarrelParser.AssignmentOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentOperator(QuarrelParser.AssignmentOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(QuarrelParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(QuarrelParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#specialArgument}.
	 * @param ctx the parse tree
	 */
	void enterSpecialArgument(QuarrelParser.SpecialArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#specialArgument}.
	 * @param ctx the parse tree
	 */
	void exitSpecialArgument(QuarrelParser.SpecialArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#ioLiteral}.
	 * @param ctx the parse tree
	 */
	void enterIoLiteral(QuarrelParser.IoLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#ioLiteral}.
	 * @param ctx the parse tree
	 */
	void exitIoLiteral(QuarrelParser.IoLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#numericLiteral}.
	 * @param ctx the parse tree
	 */
	void enterNumericLiteral(QuarrelParser.NumericLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#numericLiteral}.
	 * @param ctx the parse tree
	 */
	void exitNumericLiteral(QuarrelParser.NumericLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#containerName}.
	 * @param ctx the parse tree
	 */
	void enterContainerName(QuarrelParser.ContainerNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#containerName}.
	 * @param ctx the parse tree
	 */
	void exitContainerName(QuarrelParser.ContainerNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#container}.
	 * @param ctx the parse tree
	 */
	void enterContainer(QuarrelParser.ContainerContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#container}.
	 * @param ctx the parse tree
	 */
	void exitContainer(QuarrelParser.ContainerContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#reservedWord}.
	 * @param ctx the parse tree
	 */
	void enterReservedWord(QuarrelParser.ReservedWordContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#reservedWord}.
	 * @param ctx the parse tree
	 */
	void exitReservedWord(QuarrelParser.ReservedWordContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#gap}.
	 * @param ctx the parse tree
	 */
	void enterGap(QuarrelParser.GapContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#gap}.
	 * @param ctx the parse tree
	 */
	void exitGap(QuarrelParser.GapContext ctx);
	/**
	 * Enter a parse tree produced by {@link QuarrelParser#eos}.
	 * @param ctx the parse tree
	 */
	void enterEos(QuarrelParser.EosContext ctx);
	/**
	 * Exit a parse tree produced by {@link QuarrelParser#eos}.
	 * @param ctx the parse tree
	 */
	void exitEos(QuarrelParser.EosContext ctx);
}