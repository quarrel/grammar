// Generated from /home/jake/git/quarrel-grammar/grammar/QuarrelParser.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class QuarrelParser extends QuarrelParserBase {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		HashBangLine=1, BracketedComment=2, MultiLineComment=3, SingleLineComment=4, 
		OpenBracket=5, CloseBracket=6, OpenAngleBracket=7, CloseAngleBracket=8, 
		OpenParen=9, CloseParen=10, OpenBrace=11, CloseBrace=12, SemiColon=13, 
		Comma=14, QuestionMark=15, QuestionMarkQuestionMark=16, Colon=17, QuestionMarkColon=18, 
		Dot=19, QuestionMarkDot=20, DotDot=21, Ellipsis=22, Plus=23, PlusPlus=24, 
		PlusPlusPlus=25, Minus=26, MinusMinus=27, MinusMinusMinus=28, Star=29, 
		StarStar=30, StarStarStar=31, BSlash=32, FSlash=33, FSlashFSlash=34, FSlashFSlashFSlash=35, 
		Percent=36, PercentPercent=37, PercentPercentPercent=38, GtGt=39, GtGtGt=40, 
		LtLt=41, LtLtLt=42, GtEq=43, LtEq=44, LtGt=45, EqEq=46, EqEqEq=47, Pipe=48, 
		PipePipe=49, PipePipePipe=50, Amp=51, AmpAmp=52, AmpAmpAmp=53, Bang=54, 
		BangBang=55, BangBangBang=56, Tilde=57, Underscore=58, QuestionMarkUnderscore=59, 
		UnderscoreColon=60, ColonUnderscore=61, At=62, AtAt=63, AtAtAt=64, TildeAt=65, 
		BangAt=66, QuestionMarkAt=67, Dollar=68, DollarDollar=69, DollarDollarDollar=70, 
		TildeDollar=71, BangDollar=72, QuestionDollar=73, Caret=74, CaretCaret=75, 
		CaretCaretCaret=76, BackTick=77, DotEq=78, ColonEq=79, QuestionMarkEq=80, 
		FSlashEq=81, CaretEq=82, PipeEq=83, BSlashEq=84, LtDash=85, DashGt=86, 
		DollarGt=87, LtDollar=88, GtDash=89, DashLt=90, GtGtDash=91, DashLtLt=92, 
		PipeGt=93, PipePipeGt=94, PipePipePipeGt=95, LtPipe=96, LtPipePipe=97, 
		LtPipePipePipe=98, EqGt=99, TildeGt=100, StarGt=101, PipeDashPipe=102, 
		PipeEqPipe=103, FSlashEqFSlash=104, NullLiteral=105, BooleanLiteral=106, 
		DecimalLiteral=107, HexLiteral=108, OctalLiteral=109, BinaryLiteral=110, 
		Container=111, TextLiteral=112, LineTerminator=113, Space=114, UnexpectedCharacter=115;
	public static final int
		RULE_program = 0, RULE_sourceElements = 1, RULE_sourceElement = 2, RULE_expressionStatement = 3, 
		RULE_compoundLiteral = 4, RULE_sequenceElements = 5, RULE_listElements = 6, 
		RULE_listElement = 7, RULE_recordElements = 8, RULE_patternLiteral = 9, 
		RULE_patternElements = 10, RULE_parameter = 11, RULE_fieldOperator = 12, 
		RULE_fieldAssignment = 13, RULE_fieldName = 14, RULE_invocationExpression = 15, 
		RULE_invocable = 16, RULE_argumentElements = 17, RULE_arguments = 18, 
		RULE_nonBinaryExpression = 19, RULE_singleExpression = 20, RULE_leftTransform = 21, 
		RULE_rightTransform = 22, RULE_formalRoutineExpression = 23, RULE_routinePattern = 24, 
		RULE_routineBody = 25, RULE_assignmentOperator = 26, RULE_literal = 27, 
		RULE_specialArgument = 28, RULE_ioLiteral = 29, RULE_numericLiteral = 30, 
		RULE_containerName = 31, RULE_container = 32, RULE_reservedWord = 33, 
		RULE_gap = 34, RULE_eos = 35;
	private static String[] makeRuleNames() {
		return new String[] {
			"program", "sourceElements", "sourceElement", "expressionStatement", 
			"compoundLiteral", "sequenceElements", "listElements", "listElement", 
			"recordElements", "patternLiteral", "patternElements", "parameter", "fieldOperator", 
			"fieldAssignment", "fieldName", "invocationExpression", "invocable", 
			"argumentElements", "arguments", "nonBinaryExpression", "singleExpression", 
			"leftTransform", "rightTransform", "formalRoutineExpression", "routinePattern", 
			"routineBody", "assignmentOperator", "literal", "specialArgument", "ioLiteral", 
			"numericLiteral", "containerName", "container", "reservedWord", "gap", 
			"eos"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, "'['", "']'", "'<'", "'>'", "'('", "')'", 
			"'{'", "'}'", "';'", "','", "'?'", "'??'", "':'", "'?:'", "'.'", "'?.'", 
			"'..'", "'...'", "'+'", "'++'", "'+++'", "'-'", "'--'", "'---'", "'*'", 
			"'**'", "'***'", "'\\'", "'/'", "'//'", "'///'", "'%'", "'%%'", "'%%%'", 
			"'>>'", "'>>>'", "'<<'", "'<<<'", "'>='", "'<='", "'<>'", "'=='", "'==='", 
			"'|'", "'||'", "'|||'", "'&'", "'&&'", "'&&&'", "'!'", "'!!'", "'!!!'", 
			"'~'", null, null, "'_:'", "':_'", "'@'", "'@@'", "'@@@'", "'~@'", "'!@'", 
			"'?@'", "'$'", "'$$'", "'$$$'", "'~$'", "'!$'", "'?$'", "'^'", "'^^'", 
			"'^^^'", "'`'", "'.='", "':='", "'?='", "'/='", "'^='", "'|='", "'\\='", 
			"'<-'", "'->'", "'$>'", "'<$'", "'>-'", "'-<'", "'>>-'", "'-<<'", "'|>'", 
			"'||>'", "'|||>'", "'<|'", "'<||'", "'<|||'", "'=>'", "'~>'", "'*>'", 
			"'|-|'", "'|=|'", "'/=/'", "'()'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "HashBangLine", "BracketedComment", "MultiLineComment", "SingleLineComment", 
			"OpenBracket", "CloseBracket", "OpenAngleBracket", "CloseAngleBracket", 
			"OpenParen", "CloseParen", "OpenBrace", "CloseBrace", "SemiColon", "Comma", 
			"QuestionMark", "QuestionMarkQuestionMark", "Colon", "QuestionMarkColon", 
			"Dot", "QuestionMarkDot", "DotDot", "Ellipsis", "Plus", "PlusPlus", "PlusPlusPlus", 
			"Minus", "MinusMinus", "MinusMinusMinus", "Star", "StarStar", "StarStarStar", 
			"BSlash", "FSlash", "FSlashFSlash", "FSlashFSlashFSlash", "Percent", 
			"PercentPercent", "PercentPercentPercent", "GtGt", "GtGtGt", "LtLt", 
			"LtLtLt", "GtEq", "LtEq", "LtGt", "EqEq", "EqEqEq", "Pipe", "PipePipe", 
			"PipePipePipe", "Amp", "AmpAmp", "AmpAmpAmp", "Bang", "BangBang", "BangBangBang", 
			"Tilde", "Underscore", "QuestionMarkUnderscore", "UnderscoreColon", "ColonUnderscore", 
			"At", "AtAt", "AtAtAt", "TildeAt", "BangAt", "QuestionMarkAt", "Dollar", 
			"DollarDollar", "DollarDollarDollar", "TildeDollar", "BangDollar", "QuestionDollar", 
			"Caret", "CaretCaret", "CaretCaretCaret", "BackTick", "DotEq", "ColonEq", 
			"QuestionMarkEq", "FSlashEq", "CaretEq", "PipeEq", "BSlashEq", "LtDash", 
			"DashGt", "DollarGt", "LtDollar", "GtDash", "DashLt", "GtGtDash", "DashLtLt", 
			"PipeGt", "PipePipeGt", "PipePipePipeGt", "LtPipe", "LtPipePipe", "LtPipePipePipe", 
			"EqGt", "TildeGt", "StarGt", "PipeDashPipe", "PipeEqPipe", "FSlashEqFSlash", 
			"NullLiteral", "BooleanLiteral", "DecimalLiteral", "HexLiteral", "OctalLiteral", 
			"BinaryLiteral", "Container", "TextLiteral", "LineTerminator", "Space", 
			"UnexpectedCharacter"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "QuarrelParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public QuarrelParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgramContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(QuarrelParser.EOF, 0); }
		public TerminalNode HashBangLine() { return getToken(QuarrelParser.HashBangLine, 0); }
		public SourceElementsContext sourceElements() {
			return getRuleContext(SourceElementsContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==HashBangLine) {
				{
				setState(72);
				match(HashBangLine);
				}
			}

			setState(76);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OpenBracket) | (1L << OpenAngleBracket) | (1L << OpenParen) | (1L << OpenBrace) | (1L << Comma) | (1L << Colon) | (1L << QuestionMarkColon) | (1L << Dot) | (1L << QuestionMarkDot) | (1L << DotDot) | (1L << Plus) | (1L << Minus) | (1L << Star) | (1L << BSlash) | (1L << Tilde) | (1L << Underscore) | (1L << QuestionMarkUnderscore))) != 0) || ((((_la - 102)) & ~0x3f) == 0 && ((1L << (_la - 102)) & ((1L << (PipeDashPipe - 102)) | (1L << (PipeEqPipe - 102)) | (1L << (FSlashEqFSlash - 102)) | (1L << (NullLiteral - 102)) | (1L << (BooleanLiteral - 102)) | (1L << (DecimalLiteral - 102)) | (1L << (HexLiteral - 102)) | (1L << (OctalLiteral - 102)) | (1L << (BinaryLiteral - 102)) | (1L << (Container - 102)) | (1L << (TextLiteral - 102)) | (1L << (LineTerminator - 102)) | (1L << (Space - 102)))) != 0)) {
				{
				setState(75);
				sourceElements();
				}
			}

			setState(78);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SourceElementsContext extends ParserRuleContext {
		public List<SourceElementContext> sourceElement() {
			return getRuleContexts(SourceElementContext.class);
		}
		public SourceElementContext sourceElement(int i) {
			return getRuleContext(SourceElementContext.class,i);
		}
		public SourceElementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sourceElements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterSourceElements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitSourceElements(this);
		}
	}

	public final SourceElementsContext sourceElements() throws RecognitionException {
		SourceElementsContext _localctx = new SourceElementsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_sourceElements);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(81); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(80);
					sourceElement();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(83); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SourceElementContext extends ParserRuleContext {
		public ExpressionStatementContext expressionStatement() {
			return getRuleContext(ExpressionStatementContext.class,0);
		}
		public TerminalNode LineTerminator() { return getToken(QuarrelParser.LineTerminator, 0); }
		public SourceElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sourceElement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterSourceElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitSourceElement(this);
		}
	}

	public final SourceElementContext sourceElement() throws RecognitionException {
		SourceElementContext _localctx = new SourceElementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_sourceElement);
		try {
			setState(87);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OpenBracket:
			case OpenAngleBracket:
			case OpenParen:
			case OpenBrace:
			case Comma:
			case Colon:
			case QuestionMarkColon:
			case Dot:
			case QuestionMarkDot:
			case DotDot:
			case Plus:
			case Minus:
			case Star:
			case BSlash:
			case Tilde:
			case Underscore:
			case QuestionMarkUnderscore:
			case PipeDashPipe:
			case PipeEqPipe:
			case FSlashEqFSlash:
			case NullLiteral:
			case BooleanLiteral:
			case DecimalLiteral:
			case HexLiteral:
			case OctalLiteral:
			case BinaryLiteral:
			case Container:
			case TextLiteral:
			case Space:
				enterOuterAlt(_localctx, 1);
				{
				setState(85);
				expressionStatement();
				}
				break;
			case LineTerminator:
				enterOuterAlt(_localctx, 2);
				{
				setState(86);
				match(LineTerminator);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionStatementContext extends ParserRuleContext {
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public ExpressionStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterExpressionStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitExpressionStatement(this);
		}
	}

	public final ExpressionStatementContext expressionStatement() throws RecognitionException {
		ExpressionStatementContext _localctx = new ExpressionStatementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_expressionStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			singleExpression(0);
			setState(90);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompoundLiteralContext extends ParserRuleContext {
		public TerminalNode OpenParen() { return getToken(QuarrelParser.OpenParen, 0); }
		public List<GapContext> gap() {
			return getRuleContexts(GapContext.class);
		}
		public GapContext gap(int i) {
			return getRuleContext(GapContext.class,i);
		}
		public SequenceElementsContext sequenceElements() {
			return getRuleContext(SequenceElementsContext.class,0);
		}
		public TerminalNode CloseParen() { return getToken(QuarrelParser.CloseParen, 0); }
		public TerminalNode OpenBracket() { return getToken(QuarrelParser.OpenBracket, 0); }
		public ListElementsContext listElements() {
			return getRuleContext(ListElementsContext.class,0);
		}
		public TerminalNode CloseBracket() { return getToken(QuarrelParser.CloseBracket, 0); }
		public TerminalNode OpenAngleBracket() { return getToken(QuarrelParser.OpenAngleBracket, 0); }
		public RecordElementsContext recordElements() {
			return getRuleContext(RecordElementsContext.class,0);
		}
		public TerminalNode CloseAngleBracket() { return getToken(QuarrelParser.CloseAngleBracket, 0); }
		public CompoundLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compoundLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterCompoundLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitCompoundLiteral(this);
		}
	}

	public final CompoundLiteralContext compoundLiteral() throws RecognitionException {
		CompoundLiteralContext _localctx = new CompoundLiteralContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_compoundLiteral);
		try {
			setState(110);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OpenParen:
				enterOuterAlt(_localctx, 1);
				{
				setState(92);
				match(OpenParen);
				setState(93);
				gap();
				setState(94);
				sequenceElements();
				setState(95);
				gap();
				setState(96);
				match(CloseParen);
				}
				break;
			case OpenBracket:
				enterOuterAlt(_localctx, 2);
				{
				setState(98);
				match(OpenBracket);
				setState(99);
				gap();
				setState(100);
				listElements();
				setState(101);
				gap();
				setState(102);
				match(CloseBracket);
				}
				break;
			case OpenAngleBracket:
				enterOuterAlt(_localctx, 3);
				{
				setState(104);
				match(OpenAngleBracket);
				setState(105);
				gap();
				setState(106);
				recordElements();
				setState(107);
				gap();
				setState(108);
				match(CloseAngleBracket);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SequenceElementsContext extends ParserRuleContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(QuarrelParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(QuarrelParser.Comma, i);
		}
		public List<TerminalNode> LineTerminator() { return getTokens(QuarrelParser.LineTerminator); }
		public TerminalNode LineTerminator(int i) {
			return getToken(QuarrelParser.LineTerminator, i);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public SequenceElementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sequenceElements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterSequenceElements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitSequenceElements(this);
		}
	}

	public final SequenceElementsContext sequenceElements() throws RecognitionException {
		SequenceElementsContext _localctx = new SequenceElementsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_sequenceElements);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(112);
			singleExpression(0);
			setState(129);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(116);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==Space) {
						{
						{
						setState(113);
						match(Space);
						}
						}
						setState(118);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(119);
					_la = _input.LA(1);
					if ( !(_la==Comma || _la==LineTerminator) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(123);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(120);
							match(Space);
							}
							} 
						}
						setState(125);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
					}
					setState(126);
					singleExpression(0);
					}
					} 
				}
				setState(131);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListElementsContext extends ParserRuleContext {
		public List<ListElementContext> listElement() {
			return getRuleContexts(ListElementContext.class);
		}
		public ListElementContext listElement(int i) {
			return getRuleContext(ListElementContext.class,i);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public List<TerminalNode> Comma() { return getTokens(QuarrelParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(QuarrelParser.Comma, i);
		}
		public List<TerminalNode> LineTerminator() { return getTokens(QuarrelParser.LineTerminator); }
		public TerminalNode LineTerminator(int i) {
			return getToken(QuarrelParser.LineTerminator, i);
		}
		public ListElementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listElements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterListElements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitListElements(this);
		}
	}

	public final ListElementsContext listElements() throws RecognitionException {
		ListElementsContext _localctx = new ListElementsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_listElements);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(133);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				{
				setState(132);
				listElement();
				}
				break;
			}
			setState(151);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(138);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==Space) {
						{
						{
						setState(135);
						match(Space);
						}
						}
						setState(140);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(141);
					_la = _input.LA(1);
					if ( !(_la==Comma || _la==LineTerminator) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(145);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(142);
							match(Space);
							}
							} 
						}
						setState(147);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
					}
					setState(148);
					listElement();
					}
					} 
				}
				setState(153);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			}
			setState(157);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(154);
					match(Space);
					}
					} 
				}
				setState(159);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			setState(163);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Comma) {
				{
				{
				setState(160);
				match(Comma);
				}
				}
				setState(165);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListElementContext extends ParserRuleContext {
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public TerminalNode Ellipsis() { return getToken(QuarrelParser.Ellipsis, 0); }
		public ListElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listElement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterListElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitListElement(this);
		}
	}

	public final ListElementContext listElement() throws RecognitionException {
		ListElementContext _localctx = new ListElementContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_listElement);
		int _la;
		try {
			setState(174);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(167);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Ellipsis) {
					{
					setState(166);
					match(Ellipsis);
					}
				}

				setState(169);
				singleExpression(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(170);
				singleExpression(0);
				setState(172);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Ellipsis) {
					{
					setState(171);
					match(Ellipsis);
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RecordElementsContext extends ParserRuleContext {
		public List<FieldAssignmentContext> fieldAssignment() {
			return getRuleContexts(FieldAssignmentContext.class);
		}
		public FieldAssignmentContext fieldAssignment(int i) {
			return getRuleContext(FieldAssignmentContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(QuarrelParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(QuarrelParser.Comma, i);
		}
		public List<TerminalNode> LineTerminator() { return getTokens(QuarrelParser.LineTerminator); }
		public TerminalNode LineTerminator(int i) {
			return getToken(QuarrelParser.LineTerminator, i);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public RecordElementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_recordElements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterRecordElements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitRecordElements(this);
		}
	}

	public final RecordElementsContext recordElements() throws RecognitionException {
		RecordElementsContext _localctx = new RecordElementsContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_recordElements);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(176);
			fieldAssignment();
			setState(193);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(180);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==Space) {
						{
						{
						setState(177);
						match(Space);
						}
						}
						setState(182);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(183);
					_la = _input.LA(1);
					if ( !(_la==Comma || _la==LineTerminator) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(187);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(184);
							match(Space);
							}
							} 
						}
						setState(189);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
					}
					setState(190);
					fieldAssignment();
					}
					} 
				}
				setState(195);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PatternLiteralContext extends ParserRuleContext {
		public TerminalNode OpenBrace() { return getToken(QuarrelParser.OpenBrace, 0); }
		public PatternElementsContext patternElements() {
			return getRuleContext(PatternElementsContext.class,0);
		}
		public TerminalNode CloseBrace() { return getToken(QuarrelParser.CloseBrace, 0); }
		public PatternLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_patternLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterPatternLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitPatternLiteral(this);
		}
	}

	public final PatternLiteralContext patternLiteral() throws RecognitionException {
		PatternLiteralContext _localctx = new PatternLiteralContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_patternLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196);
			match(OpenBrace);
			setState(197);
			patternElements();
			setState(198);
			match(CloseBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PatternElementsContext extends ParserRuleContext {
		public List<TerminalNode> Comma() { return getTokens(QuarrelParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(QuarrelParser.Comma, i);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public PatternElementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_patternElements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterPatternElements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitPatternElements(this);
		}
	}

	public final PatternElementsContext patternElements() throws RecognitionException {
		PatternElementsContext _localctx = new PatternElementsContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_patternElements);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(203);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(200);
					match(Comma);
					}
					} 
				}
				setState(205);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			}
			setState(209);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(206);
					match(Space);
					}
					} 
				}
				setState(211);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			setState(213);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				{
				setState(212);
				parameter();
				}
				break;
			}
			setState(218);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(215);
					match(Space);
					}
					} 
				}
				setState(220);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			setState(231);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(221);
					match(Comma);
					setState(225);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(222);
							match(Space);
							}
							} 
						}
						setState(227);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
					}
					setState(228);
					parameter();
					}
					} 
				}
				setState(233);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			}
			setState(237);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(234);
					match(Space);
					}
					} 
				}
				setState(239);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			}
			setState(243);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Comma) {
				{
				{
				setState(240);
				match(Comma);
				}
				}
				setState(245);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public ContainerContext container() {
			return getRuleContext(ContainerContext.class,0);
		}
		public CompoundLiteralContext compoundLiteral() {
			return getRuleContext(CompoundLiteralContext.class,0);
		}
		public FieldAssignmentContext fieldAssignment() {
			return getRuleContext(FieldAssignmentContext.class,0);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitParameter(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_parameter);
		try {
			setState(249);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(246);
				container();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(247);
				compoundLiteral();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(248);
				fieldAssignment();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldOperatorContext extends ParserRuleContext {
		public TerminalNode Dot() { return getToken(QuarrelParser.Dot, 0); }
		public TerminalNode Colon() { return getToken(QuarrelParser.Colon, 0); }
		public TerminalNode QuestionMarkDot() { return getToken(QuarrelParser.QuestionMarkDot, 0); }
		public TerminalNode QuestionMarkColon() { return getToken(QuarrelParser.QuestionMarkColon, 0); }
		public FieldOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterFieldOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitFieldOperator(this);
		}
	}

	public final FieldOperatorContext fieldOperator() throws RecognitionException {
		FieldOperatorContext _localctx = new FieldOperatorContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_fieldOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(251);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Colon) | (1L << QuestionMarkColon) | (1L << Dot) | (1L << QuestionMarkDot))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldAssignmentContext extends ParserRuleContext {
		public FieldAssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldAssignment; }
	 
		public FieldAssignmentContext() { }
		public void copyFrom(FieldAssignmentContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FieldExpressionAssignmentContext extends FieldAssignmentContext {
		public FieldNameContext fieldName() {
			return getRuleContext(FieldNameContext.class,0);
		}
		public FieldOperatorContext fieldOperator() {
			return getRuleContext(FieldOperatorContext.class,0);
		}
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public FieldExpressionAssignmentContext(FieldAssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterFieldExpressionAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitFieldExpressionAssignment(this);
		}
	}
	public static class ComputedFieldExpressionAssignmentContext extends FieldAssignmentContext {
		public TerminalNode OpenAngleBracket() { return getToken(QuarrelParser.OpenAngleBracket, 0); }
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode CloseAngleBracket() { return getToken(QuarrelParser.CloseAngleBracket, 0); }
		public FieldOperatorContext fieldOperator() {
			return getRuleContext(FieldOperatorContext.class,0);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public ComputedFieldExpressionAssignmentContext(FieldAssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterComputedFieldExpressionAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitComputedFieldExpressionAssignment(this);
		}
	}
	public static class PriorContainerFieldAssignmentContext extends FieldAssignmentContext {
		public FieldOperatorContext fieldOperator() {
			return getRuleContext(FieldOperatorContext.class,0);
		}
		public FieldNameContext fieldName() {
			return getRuleContext(FieldNameContext.class,0);
		}
		public PriorContainerFieldAssignmentContext(FieldAssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterPriorContainerFieldAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitPriorContainerFieldAssignment(this);
		}
	}
	public static class RoutineFieldContext extends FieldAssignmentContext {
		public FieldNameContext fieldName() {
			return getRuleContext(FieldNameContext.class,0);
		}
		public FieldOperatorContext fieldOperator() {
			return getRuleContext(FieldOperatorContext.class,0);
		}
		public FormalRoutineExpressionContext formalRoutineExpression() {
			return getRuleContext(FormalRoutineExpressionContext.class,0);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public RoutineFieldContext(FieldAssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterRoutineField(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitRoutineField(this);
		}
	}

	public final FieldAssignmentContext fieldAssignment() throws RecognitionException {
		FieldAssignmentContext _localctx = new FieldAssignmentContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_fieldAssignment);
		int _la;
		try {
			int _alt;
			setState(300);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				_localctx = new PriorContainerFieldAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(253);
				fieldOperator();
				setState(254);
				fieldName();
				}
				break;
			case 2:
				_localctx = new FieldExpressionAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(256);
				fieldName();
				setState(257);
				fieldOperator();
				setState(261);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(258);
						match(Space);
						}
						} 
					}
					setState(263);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
				}
				setState(264);
				singleExpression(0);
				}
				break;
			case 3:
				_localctx = new ComputedFieldExpressionAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(266);
				match(OpenAngleBracket);
				setState(270);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(267);
						match(Space);
						}
						} 
					}
					setState(272);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				}
				setState(273);
				singleExpression(0);
				setState(277);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(274);
					match(Space);
					}
					}
					setState(279);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(280);
				match(CloseAngleBracket);
				setState(281);
				fieldOperator();
				setState(285);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(282);
						match(Space);
						}
						} 
					}
					setState(287);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
				}
				setState(288);
				singleExpression(0);
				}
				break;
			case 4:
				_localctx = new RoutineFieldContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(290);
				fieldName();
				setState(291);
				fieldOperator();
				setState(295);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(292);
						match(Space);
						}
						} 
					}
					setState(297);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
				}
				setState(298);
				formalRoutineExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldNameContext extends ParserRuleContext {
		public ContainerNameContext containerName() {
			return getRuleContext(ContainerNameContext.class,0);
		}
		public TerminalNode TextLiteral() { return getToken(QuarrelParser.TextLiteral, 0); }
		public NumericLiteralContext numericLiteral() {
			return getRuleContext(NumericLiteralContext.class,0);
		}
		public TerminalNode OpenBracket() { return getToken(QuarrelParser.OpenBracket, 0); }
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public TerminalNode CloseBracket() { return getToken(QuarrelParser.CloseBracket, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public FieldNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterFieldName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitFieldName(this);
		}
	}

	public final FieldNameContext fieldName() throws RecognitionException {
		FieldNameContext _localctx = new FieldNameContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_fieldName);
		int _la;
		try {
			int _alt;
			setState(321);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BSlash:
			case BooleanLiteral:
			case Container:
			case Space:
				enterOuterAlt(_localctx, 1);
				{
				setState(302);
				containerName();
				}
				break;
			case TextLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(303);
				match(TextLiteral);
				}
				break;
			case DecimalLiteral:
			case HexLiteral:
			case OctalLiteral:
			case BinaryLiteral:
				enterOuterAlt(_localctx, 3);
				{
				setState(304);
				numericLiteral();
				}
				break;
			case OpenBracket:
				enterOuterAlt(_localctx, 4);
				{
				setState(305);
				match(OpenBracket);
				setState(309);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,35,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(306);
						match(Space);
						}
						} 
					}
					setState(311);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,35,_ctx);
				}
				setState(312);
				singleExpression(0);
				setState(316);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(313);
					match(Space);
					}
					}
					setState(318);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(319);
				match(CloseBracket);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InvocationExpressionContext extends ParserRuleContext {
		public InvocableContext invocable() {
			return getRuleContext(InvocableContext.class,0);
		}
		public List<ArgumentElementsContext> argumentElements() {
			return getRuleContexts(ArgumentElementsContext.class);
		}
		public ArgumentElementsContext argumentElements(int i) {
			return getRuleContext(ArgumentElementsContext.class,i);
		}
		public InvocationExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_invocationExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterInvocationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitInvocationExpression(this);
		}
	}

	public final InvocationExpressionContext invocationExpression() throws RecognitionException {
		InvocationExpressionContext _localctx = new InvocationExpressionContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_invocationExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(323);
			invocable();
			setState(325); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(324);
					argumentElements();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(327); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InvocableContext extends ParserRuleContext {
		public CompoundLiteralContext compoundLiteral() {
			return getRuleContext(CompoundLiteralContext.class,0);
		}
		public ContainerContext container() {
			return getRuleContext(ContainerContext.class,0);
		}
		public InvocableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_invocable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterInvocable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitInvocable(this);
		}
	}

	public final InvocableContext invocable() throws RecognitionException {
		InvocableContext _localctx = new InvocableContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_invocable);
		try {
			setState(331);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OpenBracket:
			case OpenAngleBracket:
			case OpenParen:
				enterOuterAlt(_localctx, 1);
				{
				setState(329);
				compoundLiteral();
				}
				break;
			case BSlash:
			case Container:
			case Space:
				enterOuterAlt(_localctx, 2);
				{
				setState(330);
				container();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentElementsContext extends ParserRuleContext {
		public TerminalNode Bang() { return getToken(QuarrelParser.Bang, 0); }
		public TerminalNode OpenBracket() { return getToken(QuarrelParser.OpenBracket, 0); }
		public TerminalNode CloseBracket() { return getToken(QuarrelParser.CloseBracket, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public TerminalNode QuestionMark() { return getToken(QuarrelParser.QuestionMark, 0); }
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public TerminalNode OpenAngleBracket() { return getToken(QuarrelParser.OpenAngleBracket, 0); }
		public TerminalNode CloseAngleBracket() { return getToken(QuarrelParser.CloseAngleBracket, 0); }
		public TerminalNode ColonUnderscore() { return getToken(QuarrelParser.ColonUnderscore, 0); }
		public TerminalNode UnderscoreColon() { return getToken(QuarrelParser.UnderscoreColon, 0); }
		public ArgumentElementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argumentElements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterArgumentElements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitArgumentElements(this);
		}
	}

	public final ArgumentElementsContext argumentElements() throws RecognitionException {
		ArgumentElementsContext _localctx = new ArgumentElementsContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_argumentElements);
		int _la;
		try {
			int _alt;
			setState(418);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(333);
				match(Bang);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(334);
				match(OpenBracket);
				setState(338);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(335);
					match(Space);
					}
					}
					setState(340);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(341);
				match(CloseBracket);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(342);
				match(OpenBracket);
				setState(346);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(343);
					match(Space);
					}
					}
					setState(348);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(349);
				match(QuestionMark);
				setState(353);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(350);
					match(Space);
					}
					}
					setState(355);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(356);
				match(CloseBracket);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(357);
				match(OpenBracket);
				setState(361);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,43,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(358);
						match(Space);
						}
						} 
					}
					setState(363);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,43,_ctx);
				}
				setState(364);
				arguments();
				setState(368);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(365);
					match(Space);
					}
					}
					setState(370);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(371);
				match(CloseBracket);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(373);
				match(OpenAngleBracket);
				setState(377);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(374);
					match(Space);
					}
					}
					setState(379);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(380);
				match(CloseBracket);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(381);
				match(OpenAngleBracket);
				setState(385);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(382);
					match(Space);
					}
					}
					setState(387);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(388);
				_la = _input.LA(1);
				if ( !(_la==UnderscoreColon || _la==ColonUnderscore) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(392);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(389);
					match(Space);
					}
					}
					setState(394);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(395);
				match(CloseAngleBracket);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(396);
				match(OpenAngleBracket);
				setState(400);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,48,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(397);
						match(Space);
						}
						} 
					}
					setState(402);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,48,_ctx);
				}
				setState(403);
				arguments();
				setState(407);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(404);
					match(Space);
					}
					}
					setState(409);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(410);
				match(CloseBracket);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(413); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(412);
						match(Space);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(415); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,50,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(417);
				arguments();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentsContext extends ParserRuleContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public List<FieldAssignmentContext> fieldAssignment() {
			return getRuleContexts(FieldAssignmentContext.class);
		}
		public FieldAssignmentContext fieldAssignment(int i) {
			return getRuleContext(FieldAssignmentContext.class,i);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public List<TerminalNode> Comma() { return getTokens(QuarrelParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(QuarrelParser.Comma, i);
		}
		public ArgumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arguments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterArguments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitArguments(this);
		}
	}

	public final ArgumentsContext arguments() throws RecognitionException {
		ArgumentsContext _localctx = new ArgumentsContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_arguments);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(422);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,52,_ctx) ) {
			case 1:
				{
				setState(420);
				singleExpression(0);
				}
				break;
			case 2:
				{
				setState(421);
				fieldAssignment();
				}
				break;
			}
			setState(427);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,53,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(424);
					match(Space);
					}
					} 
				}
				setState(429);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,53,_ctx);
			}
			setState(443);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,56,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(430);
					match(Comma);
					setState(434);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(431);
							match(Space);
							}
							} 
						}
						setState(436);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
					}
					setState(439);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
					case 1:
						{
						setState(437);
						singleExpression(0);
						}
						break;
					case 2:
						{
						setState(438);
						fieldAssignment();
						}
						break;
					}
					}
					} 
				}
				setState(445);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,56,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NonBinaryExpressionContext extends ParserRuleContext {
		public NonBinaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nonBinaryExpression; }
	 
		public NonBinaryExpressionContext() { }
		public void copyFrom(NonBinaryExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class InvokeExpressionContext extends NonBinaryExpressionContext {
		public InvocationExpressionContext invocationExpression() {
			return getRuleContext(InvocationExpressionContext.class,0);
		}
		public InvokeExpressionContext(NonBinaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterInvokeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitInvokeExpression(this);
		}
	}
	public static class LiteralExpressionContext extends NonBinaryExpressionContext {
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public LiteralExpressionContext(NonBinaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterLiteralExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitLiteralExpression(this);
		}
	}
	public static class CompoundExpressionContext extends NonBinaryExpressionContext {
		public CompoundLiteralContext compoundLiteral() {
			return getRuleContext(CompoundLiteralContext.class,0);
		}
		public CompoundExpressionContext(NonBinaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterCompoundExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitCompoundExpression(this);
		}
	}
	public static class SpecArgExpressionContext extends NonBinaryExpressionContext {
		public SpecialArgumentContext specialArgument() {
			return getRuleContext(SpecialArgumentContext.class,0);
		}
		public SpecArgExpressionContext(NonBinaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterSpecArgExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitSpecArgExpression(this);
		}
	}
	public static class ContainerExpressionContext extends NonBinaryExpressionContext {
		public ContainerContext container() {
			return getRuleContext(ContainerContext.class,0);
		}
		public ContainerExpressionContext(NonBinaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterContainerExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitContainerExpression(this);
		}
	}

	public final NonBinaryExpressionContext nonBinaryExpression() throws RecognitionException {
		NonBinaryExpressionContext _localctx = new NonBinaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_nonBinaryExpression);
		try {
			setState(451);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,57,_ctx) ) {
			case 1:
				_localctx = new LiteralExpressionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(446);
				literal();
				}
				break;
			case 2:
				_localctx = new SpecArgExpressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(447);
				specialArgument();
				}
				break;
			case 3:
				_localctx = new InvokeExpressionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(448);
				invocationExpression();
				}
				break;
			case 4:
				_localctx = new ContainerExpressionContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(449);
				container();
				}
				break;
			case 5:
				_localctx = new CompoundExpressionContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(450);
				compoundLiteral();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SingleExpressionContext extends ParserRuleContext {
		public SingleExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_singleExpression; }
	 
		public SingleExpressionContext() { }
		public void copyFrom(SingleExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BaseExpressionContext extends SingleExpressionContext {
		public NonBinaryExpressionContext nonBinaryExpression() {
			return getRuleContext(NonBinaryExpressionContext.class,0);
		}
		public BaseExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterBaseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitBaseExpression(this);
		}
	}
	public static class AdditiveExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode Plus() { return getToken(QuarrelParser.Plus, 0); }
		public TerminalNode Minus() { return getToken(QuarrelParser.Minus, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public AdditiveExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterAdditiveExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitAdditiveExpression(this);
		}
	}
	public static class RelationalExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode LtLt() { return getToken(QuarrelParser.LtLt, 0); }
		public TerminalNode GtGt() { return getToken(QuarrelParser.GtGt, 0); }
		public TerminalNode LtEq() { return getToken(QuarrelParser.LtEq, 0); }
		public TerminalNode GtEq() { return getToken(QuarrelParser.GtEq, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public RelationalExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterRelationalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitRelationalExpression(this);
		}
	}
	public static class LogicalAndExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode AmpAmp() { return getToken(QuarrelParser.AmpAmp, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public LogicalAndExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterLogicalAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitLogicalAndExpression(this);
		}
	}
	public static class UnaryPostfixExpressionContext extends SingleExpressionContext {
		public NonBinaryExpressionContext nonBinaryExpression() {
			return getRuleContext(NonBinaryExpressionContext.class,0);
		}
		public TerminalNode DotDot() { return getToken(QuarrelParser.DotDot, 0); }
		public UnaryPostfixExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterUnaryPostfixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitUnaryPostfixExpression(this);
		}
	}
	public static class ExponentiationExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode StarStar() { return getToken(QuarrelParser.StarStar, 0); }
		public TerminalNode FSlashFSlash() { return getToken(QuarrelParser.FSlashFSlash, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public ExponentiationExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterExponentiationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitExponentiationExpression(this);
		}
	}
	public static class LogicalOrExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode PipePipe() { return getToken(QuarrelParser.PipePipe, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public LogicalOrExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterLogicalOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitLogicalOrExpression(this);
		}
	}
	public static class InExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode At() { return getToken(QuarrelParser.At, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public InExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterInExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitInExpression(this);
		}
	}
	public static class TransformRightExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public RightTransformContext rightTransform() {
			return getRuleContext(RightTransformContext.class,0);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public TransformRightExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterTransformRightExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitTransformRightExpression(this);
		}
	}
	public static class TransformLeftExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public LeftTransformContext leftTransform() {
			return getRuleContext(LeftTransformContext.class,0);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public TransformLeftExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterTransformLeftExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitTransformLeftExpression(this);
		}
	}
	public static class MemberBSlashExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode BSlash() { return getToken(QuarrelParser.BSlash, 0); }
		public MemberBSlashExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterMemberBSlashExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitMemberBSlashExpression(this);
		}
	}
	public static class BitAndExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode AmpAmpAmp() { return getToken(QuarrelParser.AmpAmpAmp, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public BitAndExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterBitAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitBitAndExpression(this);
		}
	}
	public static class RoutineExpressionContext extends SingleExpressionContext {
		public FormalRoutineExpressionContext formalRoutineExpression() {
			return getRuleContext(FormalRoutineExpressionContext.class,0);
		}
		public RoutineExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterRoutineExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitRoutineExpression(this);
		}
	}
	public static class RecordAssignmentOperatorExpressionContext extends SingleExpressionContext {
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public TerminalNode BSlashEq() { return getToken(QuarrelParser.BSlashEq, 0); }
		public GapContext gap() {
			return getRuleContext(GapContext.class,0);
		}
		public RecordElementsContext recordElements() {
			return getRuleContext(RecordElementsContext.class,0);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public RecordAssignmentOperatorExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterRecordAssignmentOperatorExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitRecordAssignmentOperatorExpression(this);
		}
	}
	public static class BitOrExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode PipePipePipe() { return getToken(QuarrelParser.PipePipePipe, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public BitOrExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterBitOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitBitOrExpression(this);
		}
	}
	public static class LogicalXorExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode BangBang() { return getToken(QuarrelParser.BangBang, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public LogicalXorExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterLogicalXorExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitLogicalXorExpression(this);
		}
	}
	public static class UnaryPrefixExpressionContext extends SingleExpressionContext {
		public NonBinaryExpressionContext nonBinaryExpression() {
			return getRuleContext(NonBinaryExpressionContext.class,0);
		}
		public TerminalNode Plus() { return getToken(QuarrelParser.Plus, 0); }
		public TerminalNode Minus() { return getToken(QuarrelParser.Minus, 0); }
		public TerminalNode Tilde() { return getToken(QuarrelParser.Tilde, 0); }
		public TerminalNode Star() { return getToken(QuarrelParser.Star, 0); }
		public TerminalNode DotDot() { return getToken(QuarrelParser.DotDot, 0); }
		public UnaryPrefixExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterUnaryPrefixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitUnaryPrefixExpression(this);
		}
	}
	public static class AssignmentOperatorExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public AssignmentOperatorContext assignmentOperator() {
			return getRuleContext(AssignmentOperatorContext.class,0);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public AssignmentOperatorExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterAssignmentOperatorExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitAssignmentOperatorExpression(this);
		}
	}
	public static class BitXOrExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode BangBangBang() { return getToken(QuarrelParser.BangBangBang, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public BitXOrExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterBitXOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitBitXOrExpression(this);
		}
	}
	public static class EqualityExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode EqEq() { return getToken(QuarrelParser.EqEq, 0); }
		public TerminalNode LtGt() { return getToken(QuarrelParser.LtGt, 0); }
		public TerminalNode EqEqEq() { return getToken(QuarrelParser.EqEqEq, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public EqualityExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterEqualityExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitEqualityExpression(this);
		}
	}
	public static class ThenExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public List<GapContext> gap() {
			return getRuleContexts(GapContext.class);
		}
		public GapContext gap(int i) {
			return getRuleContext(GapContext.class,i);
		}
		public TerminalNode AtAt() { return getToken(QuarrelParser.AtAt, 0); }
		public TerminalNode TildeAt() { return getToken(QuarrelParser.TildeAt, 0); }
		public TerminalNode QuestionMarkAt() { return getToken(QuarrelParser.QuestionMarkAt, 0); }
		public ThenExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterThenExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitThenExpression(this);
		}
	}
	public static class MultiplicativeExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode Star() { return getToken(QuarrelParser.Star, 0); }
		public TerminalNode FSlash() { return getToken(QuarrelParser.FSlash, 0); }
		public TerminalNode Percent() { return getToken(QuarrelParser.Percent, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public MultiplicativeExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterMultiplicativeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitMultiplicativeExpression(this);
		}
	}
	public static class BitShiftExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public TerminalNode LtLtLt() { return getToken(QuarrelParser.LtLtLt, 0); }
		public TerminalNode GtGtGt() { return getToken(QuarrelParser.GtGtGt, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public BitShiftExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterBitShiftExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitBitShiftExpression(this);
		}
	}

	public final SingleExpressionContext singleExpression() throws RecognitionException {
		return singleExpression(0);
	}

	private SingleExpressionContext singleExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		SingleExpressionContext _localctx = new SingleExpressionContext(_ctx, _parentState);
		SingleExpressionContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, 40, RULE_singleExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(461);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,58,_ctx) ) {
			case 1:
				{
				_localctx = new BaseExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(454);
				nonBinaryExpression();
				}
				break;
			case 2:
				{
				_localctx = new UnaryPostfixExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(455);
				nonBinaryExpression();
				setState(456);
				match(DotDot);
				}
				break;
			case 3:
				{
				_localctx = new UnaryPrefixExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(458);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DotDot) | (1L << Plus) | (1L << Minus) | (1L << Star) | (1L << Tilde))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(459);
				nonBinaryExpression();
				}
				break;
			case 4:
				{
				_localctx = new RoutineExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(460);
				formalRoutineExpression();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(724);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,93,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(722);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,92,_ctx) ) {
					case 1:
						{
						_localctx = new MemberBSlashExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(463);
						if (!(precpred(_ctx, 22))) throw new FailedPredicateException(this, "precpred(_ctx, 22)");
						setState(464);
						match(BSlash);
						setState(465);
						singleExpression(23);
						}
						break;
					case 2:
						{
						_localctx = new MultiplicativeExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(466);
						if (!(precpred(_ctx, 20))) throw new FailedPredicateException(this, "precpred(_ctx, 20)");
						setState(468); 
						_errHandler.sync(this);
						_la = _input.LA(1);
						do {
							{
							{
							setState(467);
							match(Space);
							}
							}
							setState(470); 
							_errHandler.sync(this);
							_la = _input.LA(1);
						} while ( _la==Space );
						setState(472);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Star) | (1L << FSlash) | (1L << Percent))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(474); 
						_errHandler.sync(this);
						_alt = 1;
						do {
							switch (_alt) {
							case 1:
								{
								{
								setState(473);
								match(Space);
								}
								}
								break;
							default:
								throw new NoViableAltException(this);
							}
							setState(476); 
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,60,_ctx);
						} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
						setState(478);
						singleExpression(21);
						}
						break;
					case 3:
						{
						_localctx = new AdditiveExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(479);
						if (!(precpred(_ctx, 19))) throw new FailedPredicateException(this, "precpred(_ctx, 19)");
						setState(481); 
						_errHandler.sync(this);
						_la = _input.LA(1);
						do {
							{
							{
							setState(480);
							match(Space);
							}
							}
							setState(483); 
							_errHandler.sync(this);
							_la = _input.LA(1);
						} while ( _la==Space );
						setState(485);
						_la = _input.LA(1);
						if ( !(_la==Plus || _la==Minus) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(487); 
						_errHandler.sync(this);
						_alt = 1;
						do {
							switch (_alt) {
							case 1:
								{
								{
								setState(486);
								match(Space);
								}
								}
								break;
							default:
								throw new NoViableAltException(this);
							}
							setState(489); 
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,62,_ctx);
						} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
						setState(491);
						singleExpression(20);
						}
						break;
					case 4:
						{
						_localctx = new ExponentiationExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(492);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(496);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(493);
							match(Space);
							}
							}
							setState(498);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(499);
						_la = _input.LA(1);
						if ( !(_la==StarStar || _la==FSlashFSlash) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(503);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,64,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(500);
								match(Space);
								}
								} 
							}
							setState(505);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,64,_ctx);
						}
						setState(506);
						singleExpression(17);
						}
						break;
					case 5:
						{
						_localctx = new BitShiftExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(507);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(511);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(508);
							match(Space);
							}
							}
							setState(513);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(514);
						_la = _input.LA(1);
						if ( !(_la==GtGtGt || _la==LtLtLt) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(518);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,66,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(515);
								match(Space);
								}
								} 
							}
							setState(520);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,66,_ctx);
						}
						setState(521);
						singleExpression(17);
						}
						break;
					case 6:
						{
						_localctx = new BitAndExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(522);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(526);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(523);
							match(Space);
							}
							}
							setState(528);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(529);
						match(AmpAmpAmp);
						setState(533);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,68,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(530);
								match(Space);
								}
								} 
							}
							setState(535);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,68,_ctx);
						}
						setState(536);
						singleExpression(16);
						}
						break;
					case 7:
						{
						_localctx = new BitXOrExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(537);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(541);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(538);
							match(Space);
							}
							}
							setState(543);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(544);
						match(BangBangBang);
						setState(548);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,70,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(545);
								match(Space);
								}
								} 
							}
							setState(550);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,70,_ctx);
						}
						setState(551);
						singleExpression(15);
						}
						break;
					case 8:
						{
						_localctx = new BitOrExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(552);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(556);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(553);
							match(Space);
							}
							}
							setState(558);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(559);
						match(PipePipePipe);
						setState(563);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,72,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(560);
								match(Space);
								}
								} 
							}
							setState(565);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,72,_ctx);
						}
						setState(566);
						singleExpression(14);
						}
						break;
					case 9:
						{
						_localctx = new RelationalExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(567);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(571);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(568);
							match(Space);
							}
							}
							setState(573);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(574);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << GtGt) | (1L << LtLt) | (1L << GtEq) | (1L << LtEq))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(578);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,74,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(575);
								match(Space);
								}
								} 
							}
							setState(580);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,74,_ctx);
						}
						setState(581);
						singleExpression(13);
						}
						break;
					case 10:
						{
						_localctx = new LogicalAndExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(582);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(586);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(583);
							match(Space);
							}
							}
							setState(588);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(589);
						match(AmpAmp);
						setState(593);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,76,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(590);
								match(Space);
								}
								} 
							}
							setState(595);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,76,_ctx);
						}
						setState(596);
						singleExpression(12);
						}
						break;
					case 11:
						{
						_localctx = new LogicalXorExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(597);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(601);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(598);
							match(Space);
							}
							}
							setState(603);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(604);
						match(BangBang);
						setState(608);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,78,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(605);
								match(Space);
								}
								} 
							}
							setState(610);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,78,_ctx);
						}
						setState(611);
						singleExpression(11);
						}
						break;
					case 12:
						{
						_localctx = new LogicalOrExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(612);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(616);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(613);
							match(Space);
							}
							}
							setState(618);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(619);
						match(PipePipe);
						setState(623);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,80,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(620);
								match(Space);
								}
								} 
							}
							setState(625);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,80,_ctx);
						}
						setState(626);
						singleExpression(10);
						}
						break;
					case 13:
						{
						_localctx = new EqualityExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(627);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(631);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(628);
							match(Space);
							}
							}
							setState(633);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(634);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LtGt) | (1L << EqEq) | (1L << EqEqEq))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(638);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,82,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(635);
								match(Space);
								}
								} 
							}
							setState(640);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,82,_ctx);
						}
						setState(641);
						singleExpression(9);
						}
						break;
					case 14:
						{
						_localctx = new InExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(642);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(646);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(643);
							match(Space);
							}
							}
							setState(648);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(649);
						match(At);
						setState(653);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,84,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(650);
								match(Space);
								}
								} 
							}
							setState(655);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,84,_ctx);
						}
						setState(656);
						singleExpression(8);
						}
						break;
					case 15:
						{
						_localctx = new ThenExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(657);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(658);
						gap();
						setState(659);
						_la = _input.LA(1);
						if ( !(((((_la - 63)) & ~0x3f) == 0 && ((1L << (_la - 63)) & ((1L << (AtAt - 63)) | (1L << (TildeAt - 63)) | (1L << (QuestionMarkAt - 63)))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(660);
						gap();
						setState(661);
						singleExpression(7);
						}
						break;
					case 16:
						{
						_localctx = new TransformRightExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(663);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(667);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(664);
							match(Space);
							}
							}
							setState(669);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(670);
						rightTransform();
						setState(674);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,86,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(671);
								match(Space);
								}
								} 
							}
							setState(676);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,86,_ctx);
						}
						setState(677);
						singleExpression(6);
						}
						break;
					case 17:
						{
						_localctx = new TransformLeftExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(679);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(683);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(680);
							match(Space);
							}
							}
							setState(685);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(686);
						leftTransform();
						setState(690);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,88,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(687);
								match(Space);
								}
								} 
							}
							setState(692);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,88,_ctx);
						}
						setState(693);
						singleExpression(4);
						}
						break;
					case 18:
						{
						_localctx = new AssignmentOperatorExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(695);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(699);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(696);
							match(Space);
							}
							}
							setState(701);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(702);
						assignmentOperator();
						setState(706);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,90,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(703);
								match(Space);
								}
								} 
							}
							setState(708);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,90,_ctx);
						}
						setState(709);
						singleExpression(2);
						}
						break;
					case 19:
						{
						_localctx = new RecordAssignmentOperatorExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(711);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(715);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==Space) {
							{
							{
							setState(712);
							match(Space);
							}
							}
							setState(717);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(718);
						match(BSlashEq);
						setState(719);
						gap();
						setState(720);
						recordElements();
						}
						break;
					}
					} 
				}
				setState(726);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,93,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class LeftTransformContext extends ParserRuleContext {
		public TerminalNode LtDash() { return getToken(QuarrelParser.LtDash, 0); }
		public TerminalNode LtDollar() { return getToken(QuarrelParser.LtDollar, 0); }
		public TerminalNode DashLt() { return getToken(QuarrelParser.DashLt, 0); }
		public TerminalNode LtPipe() { return getToken(QuarrelParser.LtPipe, 0); }
		public TerminalNode LtPipePipe() { return getToken(QuarrelParser.LtPipePipe, 0); }
		public TerminalNode LtPipePipePipe() { return getToken(QuarrelParser.LtPipePipePipe, 0); }
		public LeftTransformContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_leftTransform; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterLeftTransform(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitLeftTransform(this);
		}
	}

	public final LeftTransformContext leftTransform() throws RecognitionException {
		LeftTransformContext _localctx = new LeftTransformContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_leftTransform);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(727);
			_la = _input.LA(1);
			if ( !(((((_la - 85)) & ~0x3f) == 0 && ((1L << (_la - 85)) & ((1L << (LtDash - 85)) | (1L << (LtDollar - 85)) | (1L << (DashLt - 85)) | (1L << (LtPipe - 85)) | (1L << (LtPipePipe - 85)) | (1L << (LtPipePipePipe - 85)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RightTransformContext extends ParserRuleContext {
		public TerminalNode DashGt() { return getToken(QuarrelParser.DashGt, 0); }
		public TerminalNode DollarGt() { return getToken(QuarrelParser.DollarGt, 0); }
		public TerminalNode GtDash() { return getToken(QuarrelParser.GtDash, 0); }
		public TerminalNode PipeGt() { return getToken(QuarrelParser.PipeGt, 0); }
		public TerminalNode PipePipeGt() { return getToken(QuarrelParser.PipePipeGt, 0); }
		public TerminalNode PipePipePipeGt() { return getToken(QuarrelParser.PipePipePipeGt, 0); }
		public RightTransformContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rightTransform; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterRightTransform(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitRightTransform(this);
		}
	}

	public final RightTransformContext rightTransform() throws RecognitionException {
		RightTransformContext _localctx = new RightTransformContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_rightTransform);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(729);
			_la = _input.LA(1);
			if ( !(((((_la - 86)) & ~0x3f) == 0 && ((1L << (_la - 86)) & ((1L << (DashGt - 86)) | (1L << (DollarGt - 86)) | (1L << (GtDash - 86)) | (1L << (PipeGt - 86)) | (1L << (PipePipeGt - 86)) | (1L << (PipePipePipeGt - 86)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalRoutineExpressionContext extends ParserRuleContext {
		public RoutinePatternContext routinePattern() {
			return getRuleContext(RoutinePatternContext.class,0);
		}
		public RoutineBodyContext routineBody() {
			return getRuleContext(RoutineBodyContext.class,0);
		}
		public TerminalNode EqGt() { return getToken(QuarrelParser.EqGt, 0); }
		public TerminalNode StarGt() { return getToken(QuarrelParser.StarGt, 0); }
		public TerminalNode TildeGt() { return getToken(QuarrelParser.TildeGt, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public FormalRoutineExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalRoutineExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterFormalRoutineExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitFormalRoutineExpression(this);
		}
	}

	public final FormalRoutineExpressionContext formalRoutineExpression() throws RecognitionException {
		FormalRoutineExpressionContext _localctx = new FormalRoutineExpressionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_formalRoutineExpression);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(731);
			routinePattern();
			setState(733); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(732);
				match(Space);
				}
				}
				setState(735); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==Space );
			setState(737);
			_la = _input.LA(1);
			if ( !(((((_la - 99)) & ~0x3f) == 0 && ((1L << (_la - 99)) & ((1L << (EqGt - 99)) | (1L << (TildeGt - 99)) | (1L << (StarGt - 99)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(739); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(738);
					match(Space);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(741); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,95,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			setState(743);
			routineBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RoutinePatternContext extends ParserRuleContext {
		public PatternLiteralContext patternLiteral() {
			return getRuleContext(PatternLiteralContext.class,0);
		}
		public PatternElementsContext patternElements() {
			return getRuleContext(PatternElementsContext.class,0);
		}
		public RoutinePatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_routinePattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterRoutinePattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitRoutinePattern(this);
		}
	}

	public final RoutinePatternContext routinePattern() throws RecognitionException {
		RoutinePatternContext _localctx = new RoutinePatternContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_routinePattern);
		try {
			setState(747);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OpenBrace:
				enterOuterAlt(_localctx, 1);
				{
				setState(745);
				patternLiteral();
				}
				break;
			case OpenBracket:
			case OpenAngleBracket:
			case OpenParen:
			case Comma:
			case Colon:
			case QuestionMarkColon:
			case Dot:
			case QuestionMarkDot:
			case BSlash:
			case BooleanLiteral:
			case DecimalLiteral:
			case HexLiteral:
			case OctalLiteral:
			case BinaryLiteral:
			case Container:
			case TextLiteral:
			case Space:
				enterOuterAlt(_localctx, 2);
				{
				setState(746);
				patternElements();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RoutineBodyContext extends ParserRuleContext {
		public TerminalNode OpenParen() { return getToken(QuarrelParser.OpenParen, 0); }
		public List<GapContext> gap() {
			return getRuleContexts(GapContext.class);
		}
		public GapContext gap(int i) {
			return getRuleContext(GapContext.class,i);
		}
		public TerminalNode CloseParen() { return getToken(QuarrelParser.CloseParen, 0); }
		public SourceElementsContext sourceElements() {
			return getRuleContext(SourceElementsContext.class,0);
		}
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public List<TerminalNode> Comma() { return getTokens(QuarrelParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(QuarrelParser.Comma, i);
		}
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public TerminalNode LineTerminator() { return getToken(QuarrelParser.LineTerminator, 0); }
		public RoutineBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_routineBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterRoutineBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitRoutineBody(this);
		}
	}

	public final RoutineBodyContext routineBody() throws RecognitionException {
		RoutineBodyContext _localctx = new RoutineBodyContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_routineBody);
		int _la;
		try {
			int _alt;
			setState(789);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,103,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(749);
				match(OpenParen);
				setState(750);
				gap();
				setState(752);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,97,_ctx) ) {
				case 1:
					{
					setState(751);
					sourceElements();
					}
					break;
				}
				setState(754);
				gap();
				setState(755);
				match(CloseParen);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(757);
				singleExpression(0);
				setState(761);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,98,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(758);
						match(Space);
						}
						} 
					}
					setState(763);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,98,_ctx);
				}
				setState(774);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(764);
						match(Comma);
						setState(768);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,99,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(765);
								match(Space);
								}
								} 
							}
							setState(770);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,99,_ctx);
						}
						setState(771);
						singleExpression(0);
						}
						} 
					}
					setState(776);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
				}
				setState(777);
				gap();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(787);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,102,_ctx) ) {
				case 1:
					{
					setState(779);
					eos();
					}
					break;
				case 2:
					{
					setState(783);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==Space) {
						{
						{
						setState(780);
						match(Space);
						}
						}
						setState(785);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(786);
					match(LineTerminator);
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentOperatorContext extends ParserRuleContext {
		public TerminalNode DotEq() { return getToken(QuarrelParser.DotEq, 0); }
		public TerminalNode ColonEq() { return getToken(QuarrelParser.ColonEq, 0); }
		public TerminalNode QuestionMarkEq() { return getToken(QuarrelParser.QuestionMarkEq, 0); }
		public TerminalNode FSlashEq() { return getToken(QuarrelParser.FSlashEq, 0); }
		public TerminalNode CaretEq() { return getToken(QuarrelParser.CaretEq, 0); }
		public TerminalNode PipeEq() { return getToken(QuarrelParser.PipeEq, 0); }
		public AssignmentOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterAssignmentOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitAssignmentOperator(this);
		}
	}

	public final AssignmentOperatorContext assignmentOperator() throws RecognitionException {
		AssignmentOperatorContext _localctx = new AssignmentOperatorContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_assignmentOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(791);
			_la = _input.LA(1);
			if ( !(((((_la - 78)) & ~0x3f) == 0 && ((1L << (_la - 78)) & ((1L << (DotEq - 78)) | (1L << (ColonEq - 78)) | (1L << (QuestionMarkEq - 78)) | (1L << (FSlashEq - 78)) | (1L << (CaretEq - 78)) | (1L << (PipeEq - 78)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public TerminalNode NullLiteral() { return getToken(QuarrelParser.NullLiteral, 0); }
		public TerminalNode BooleanLiteral() { return getToken(QuarrelParser.BooleanLiteral, 0); }
		public TerminalNode TextLiteral() { return getToken(QuarrelParser.TextLiteral, 0); }
		public IoLiteralContext ioLiteral() {
			return getRuleContext(IoLiteralContext.class,0);
		}
		public NumericLiteralContext numericLiteral() {
			return getRuleContext(NumericLiteralContext.class,0);
		}
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitLiteral(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_literal);
		try {
			setState(798);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NullLiteral:
				enterOuterAlt(_localctx, 1);
				{
				setState(793);
				match(NullLiteral);
				}
				break;
			case BooleanLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(794);
				match(BooleanLiteral);
				}
				break;
			case TextLiteral:
				enterOuterAlt(_localctx, 3);
				{
				setState(795);
				match(TextLiteral);
				}
				break;
			case PipeDashPipe:
			case PipeEqPipe:
			case FSlashEqFSlash:
				enterOuterAlt(_localctx, 4);
				{
				setState(796);
				ioLiteral();
				}
				break;
			case DecimalLiteral:
			case HexLiteral:
			case OctalLiteral:
			case BinaryLiteral:
				enterOuterAlt(_localctx, 5);
				{
				setState(797);
				numericLiteral();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SpecialArgumentContext extends ParserRuleContext {
		public TerminalNode Underscore() { return getToken(QuarrelParser.Underscore, 0); }
		public TerminalNode QuestionMarkUnderscore() { return getToken(QuarrelParser.QuestionMarkUnderscore, 0); }
		public SpecialArgumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_specialArgument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterSpecialArgument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitSpecialArgument(this);
		}
	}

	public final SpecialArgumentContext specialArgument() throws RecognitionException {
		SpecialArgumentContext _localctx = new SpecialArgumentContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_specialArgument);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(800);
			_la = _input.LA(1);
			if ( !(_la==Underscore || _la==QuestionMarkUnderscore) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IoLiteralContext extends ParserRuleContext {
		public TerminalNode PipeDashPipe() { return getToken(QuarrelParser.PipeDashPipe, 0); }
		public TerminalNode PipeEqPipe() { return getToken(QuarrelParser.PipeEqPipe, 0); }
		public TerminalNode FSlashEqFSlash() { return getToken(QuarrelParser.FSlashEqFSlash, 0); }
		public IoLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ioLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterIoLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitIoLiteral(this);
		}
	}

	public final IoLiteralContext ioLiteral() throws RecognitionException {
		IoLiteralContext _localctx = new IoLiteralContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_ioLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(802);
			_la = _input.LA(1);
			if ( !(((((_la - 102)) & ~0x3f) == 0 && ((1L << (_la - 102)) & ((1L << (PipeDashPipe - 102)) | (1L << (PipeEqPipe - 102)) | (1L << (FSlashEqFSlash - 102)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericLiteralContext extends ParserRuleContext {
		public TerminalNode DecimalLiteral() { return getToken(QuarrelParser.DecimalLiteral, 0); }
		public TerminalNode HexLiteral() { return getToken(QuarrelParser.HexLiteral, 0); }
		public TerminalNode OctalLiteral() { return getToken(QuarrelParser.OctalLiteral, 0); }
		public TerminalNode BinaryLiteral() { return getToken(QuarrelParser.BinaryLiteral, 0); }
		public NumericLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterNumericLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitNumericLiteral(this);
		}
	}

	public final NumericLiteralContext numericLiteral() throws RecognitionException {
		NumericLiteralContext _localctx = new NumericLiteralContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_numericLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(804);
			_la = _input.LA(1);
			if ( !(((((_la - 107)) & ~0x3f) == 0 && ((1L << (_la - 107)) & ((1L << (DecimalLiteral - 107)) | (1L << (HexLiteral - 107)) | (1L << (OctalLiteral - 107)) | (1L << (BinaryLiteral - 107)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContainerNameContext extends ParserRuleContext {
		public ContainerContext container() {
			return getRuleContext(ContainerContext.class,0);
		}
		public ReservedWordContext reservedWord() {
			return getRuleContext(ReservedWordContext.class,0);
		}
		public ContainerNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_containerName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterContainerName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitContainerName(this);
		}
	}

	public final ContainerNameContext containerName() throws RecognitionException {
		ContainerNameContext _localctx = new ContainerNameContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_containerName);
		try {
			setState(808);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BSlash:
			case Container:
			case Space:
				enterOuterAlt(_localctx, 1);
				{
				setState(806);
				container();
				}
				break;
			case BooleanLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(807);
				reservedWord();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContainerContext extends ParserRuleContext {
		public TerminalNode Container() { return getToken(QuarrelParser.Container, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public TerminalNode BSlash() { return getToken(QuarrelParser.BSlash, 0); }
		public ContainerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_container; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterContainer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitContainer(this);
		}
	}

	public final ContainerContext container() throws RecognitionException {
		ContainerContext _localctx = new ContainerContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_container);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(813);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Space) {
				{
				{
				setState(810);
				match(Space);
				}
				}
				setState(815);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(817);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BSlash) {
				{
				setState(816);
				match(BSlash);
				}
			}

			setState(819);
			match(Container);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReservedWordContext extends ParserRuleContext {
		public TerminalNode BooleanLiteral() { return getToken(QuarrelParser.BooleanLiteral, 0); }
		public ReservedWordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reservedWord; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterReservedWord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitReservedWord(this);
		}
	}

	public final ReservedWordContext reservedWord() throws RecognitionException {
		ReservedWordContext _localctx = new ReservedWordContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_reservedWord);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(821);
			match(BooleanLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GapContext extends ParserRuleContext {
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public List<TerminalNode> LineTerminator() { return getTokens(QuarrelParser.LineTerminator); }
		public TerminalNode LineTerminator(int i) {
			return getToken(QuarrelParser.LineTerminator, i);
		}
		public GapContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_gap; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterGap(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitGap(this);
		}
	}

	public final GapContext gap() throws RecognitionException {
		GapContext _localctx = new GapContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_gap);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(826);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,108,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(823);
					_la = _input.LA(1);
					if ( !(_la==LineTerminator || _la==Space) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					} 
				}
				setState(828);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,108,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EosContext extends ParserRuleContext {
		public TerminalNode SemiColon() { return getToken(QuarrelParser.SemiColon, 0); }
		public List<TerminalNode> Space() { return getTokens(QuarrelParser.Space); }
		public TerminalNode Space(int i) {
			return getToken(QuarrelParser.Space, i);
		}
		public TerminalNode EOF() { return getToken(QuarrelParser.EOF, 0); }
		public TerminalNode LineTerminator() { return getToken(QuarrelParser.LineTerminator, 0); }
		public EosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eos; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).enterEos(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QuarrelParserListener ) ((QuarrelParserListener)listener).exitEos(this);
		}
	}

	public final EosContext eos() throws RecognitionException {
		EosContext _localctx = new EosContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_eos);
		int _la;
		try {
			setState(850);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,112,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(832);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(829);
					match(Space);
					}
					}
					setState(834);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(835);
				match(SemiColon);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(839);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(836);
					match(Space);
					}
					}
					setState(841);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(842);
				match(EOF);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(846);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==Space) {
					{
					{
					setState(843);
					match(Space);
					}
					}
					setState(848);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(849);
				match(LineTerminator);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 20:
			return singleExpression_sempred((SingleExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean singleExpression_sempred(SingleExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 22);
		case 1:
			return precpred(_ctx, 20);
		case 2:
			return precpred(_ctx, 19);
		case 3:
			return precpred(_ctx, 17);
		case 4:
			return precpred(_ctx, 16);
		case 5:
			return precpred(_ctx, 15);
		case 6:
			return precpred(_ctx, 14);
		case 7:
			return precpred(_ctx, 13);
		case 8:
			return precpred(_ctx, 12);
		case 9:
			return precpred(_ctx, 11);
		case 10:
			return precpred(_ctx, 10);
		case 11:
			return precpred(_ctx, 9);
		case 12:
			return precpred(_ctx, 8);
		case 13:
			return precpred(_ctx, 7);
		case 14:
			return precpred(_ctx, 6);
		case 15:
			return precpred(_ctx, 5);
		case 16:
			return precpred(_ctx, 4);
		case 17:
			return precpred(_ctx, 2);
		case 18:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3u\u0357\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\3\2\5\2L\n\2\3\2\5\2O\n\2\3\2\3\2\3\3\6"+
		"\3T\n\3\r\3\16\3U\3\4\3\4\5\4Z\n\4\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6q\n\6\3\7\3\7\7\7"+
		"u\n\7\f\7\16\7x\13\7\3\7\3\7\7\7|\n\7\f\7\16\7\177\13\7\3\7\7\7\u0082"+
		"\n\7\f\7\16\7\u0085\13\7\3\b\5\b\u0088\n\b\3\b\7\b\u008b\n\b\f\b\16\b"+
		"\u008e\13\b\3\b\3\b\7\b\u0092\n\b\f\b\16\b\u0095\13\b\3\b\7\b\u0098\n"+
		"\b\f\b\16\b\u009b\13\b\3\b\7\b\u009e\n\b\f\b\16\b\u00a1\13\b\3\b\7\b\u00a4"+
		"\n\b\f\b\16\b\u00a7\13\b\3\t\5\t\u00aa\n\t\3\t\3\t\3\t\5\t\u00af\n\t\5"+
		"\t\u00b1\n\t\3\n\3\n\7\n\u00b5\n\n\f\n\16\n\u00b8\13\n\3\n\3\n\7\n\u00bc"+
		"\n\n\f\n\16\n\u00bf\13\n\3\n\7\n\u00c2\n\n\f\n\16\n\u00c5\13\n\3\13\3"+
		"\13\3\13\3\13\3\f\7\f\u00cc\n\f\f\f\16\f\u00cf\13\f\3\f\7\f\u00d2\n\f"+
		"\f\f\16\f\u00d5\13\f\3\f\5\f\u00d8\n\f\3\f\7\f\u00db\n\f\f\f\16\f\u00de"+
		"\13\f\3\f\3\f\7\f\u00e2\n\f\f\f\16\f\u00e5\13\f\3\f\7\f\u00e8\n\f\f\f"+
		"\16\f\u00eb\13\f\3\f\7\f\u00ee\n\f\f\f\16\f\u00f1\13\f\3\f\7\f\u00f4\n"+
		"\f\f\f\16\f\u00f7\13\f\3\r\3\r\3\r\5\r\u00fc\n\r\3\16\3\16\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\7\17\u0106\n\17\f\17\16\17\u0109\13\17\3\17\3\17\3"+
		"\17\3\17\7\17\u010f\n\17\f\17\16\17\u0112\13\17\3\17\3\17\7\17\u0116\n"+
		"\17\f\17\16\17\u0119\13\17\3\17\3\17\3\17\7\17\u011e\n\17\f\17\16\17\u0121"+
		"\13\17\3\17\3\17\3\17\3\17\3\17\7\17\u0128\n\17\f\17\16\17\u012b\13\17"+
		"\3\17\3\17\5\17\u012f\n\17\3\20\3\20\3\20\3\20\3\20\7\20\u0136\n\20\f"+
		"\20\16\20\u0139\13\20\3\20\3\20\7\20\u013d\n\20\f\20\16\20\u0140\13\20"+
		"\3\20\3\20\5\20\u0144\n\20\3\21\3\21\6\21\u0148\n\21\r\21\16\21\u0149"+
		"\3\22\3\22\5\22\u014e\n\22\3\23\3\23\3\23\7\23\u0153\n\23\f\23\16\23\u0156"+
		"\13\23\3\23\3\23\3\23\7\23\u015b\n\23\f\23\16\23\u015e\13\23\3\23\3\23"+
		"\7\23\u0162\n\23\f\23\16\23\u0165\13\23\3\23\3\23\3\23\7\23\u016a\n\23"+
		"\f\23\16\23\u016d\13\23\3\23\3\23\7\23\u0171\n\23\f\23\16\23\u0174\13"+
		"\23\3\23\3\23\3\23\3\23\7\23\u017a\n\23\f\23\16\23\u017d\13\23\3\23\3"+
		"\23\3\23\7\23\u0182\n\23\f\23\16\23\u0185\13\23\3\23\3\23\7\23\u0189\n"+
		"\23\f\23\16\23\u018c\13\23\3\23\3\23\3\23\7\23\u0191\n\23\f\23\16\23\u0194"+
		"\13\23\3\23\3\23\7\23\u0198\n\23\f\23\16\23\u019b\13\23\3\23\3\23\3\23"+
		"\6\23\u01a0\n\23\r\23\16\23\u01a1\3\23\5\23\u01a5\n\23\3\24\3\24\5\24"+
		"\u01a9\n\24\3\24\7\24\u01ac\n\24\f\24\16\24\u01af\13\24\3\24\3\24\7\24"+
		"\u01b3\n\24\f\24\16\24\u01b6\13\24\3\24\3\24\5\24\u01ba\n\24\7\24\u01bc"+
		"\n\24\f\24\16\24\u01bf\13\24\3\25\3\25\3\25\3\25\3\25\5\25\u01c6\n\25"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u01d0\n\26\3\26\3\26\3\26"+
		"\3\26\3\26\6\26\u01d7\n\26\r\26\16\26\u01d8\3\26\3\26\6\26\u01dd\n\26"+
		"\r\26\16\26\u01de\3\26\3\26\3\26\6\26\u01e4\n\26\r\26\16\26\u01e5\3\26"+
		"\3\26\6\26\u01ea\n\26\r\26\16\26\u01eb\3\26\3\26\3\26\7\26\u01f1\n\26"+
		"\f\26\16\26\u01f4\13\26\3\26\3\26\7\26\u01f8\n\26\f\26\16\26\u01fb\13"+
		"\26\3\26\3\26\3\26\7\26\u0200\n\26\f\26\16\26\u0203\13\26\3\26\3\26\7"+
		"\26\u0207\n\26\f\26\16\26\u020a\13\26\3\26\3\26\3\26\7\26\u020f\n\26\f"+
		"\26\16\26\u0212\13\26\3\26\3\26\7\26\u0216\n\26\f\26\16\26\u0219\13\26"+
		"\3\26\3\26\3\26\7\26\u021e\n\26\f\26\16\26\u0221\13\26\3\26\3\26\7\26"+
		"\u0225\n\26\f\26\16\26\u0228\13\26\3\26\3\26\3\26\7\26\u022d\n\26\f\26"+
		"\16\26\u0230\13\26\3\26\3\26\7\26\u0234\n\26\f\26\16\26\u0237\13\26\3"+
		"\26\3\26\3\26\7\26\u023c\n\26\f\26\16\26\u023f\13\26\3\26\3\26\7\26\u0243"+
		"\n\26\f\26\16\26\u0246\13\26\3\26\3\26\3\26\7\26\u024b\n\26\f\26\16\26"+
		"\u024e\13\26\3\26\3\26\7\26\u0252\n\26\f\26\16\26\u0255\13\26\3\26\3\26"+
		"\3\26\7\26\u025a\n\26\f\26\16\26\u025d\13\26\3\26\3\26\7\26\u0261\n\26"+
		"\f\26\16\26\u0264\13\26\3\26\3\26\3\26\7\26\u0269\n\26\f\26\16\26\u026c"+
		"\13\26\3\26\3\26\7\26\u0270\n\26\f\26\16\26\u0273\13\26\3\26\3\26\3\26"+
		"\7\26\u0278\n\26\f\26\16\26\u027b\13\26\3\26\3\26\7\26\u027f\n\26\f\26"+
		"\16\26\u0282\13\26\3\26\3\26\3\26\7\26\u0287\n\26\f\26\16\26\u028a\13"+
		"\26\3\26\3\26\7\26\u028e\n\26\f\26\16\26\u0291\13\26\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\26\3\26\7\26\u029c\n\26\f\26\16\26\u029f\13\26\3"+
		"\26\3\26\7\26\u02a3\n\26\f\26\16\26\u02a6\13\26\3\26\3\26\3\26\3\26\7"+
		"\26\u02ac\n\26\f\26\16\26\u02af\13\26\3\26\3\26\7\26\u02b3\n\26\f\26\16"+
		"\26\u02b6\13\26\3\26\3\26\3\26\3\26\7\26\u02bc\n\26\f\26\16\26\u02bf\13"+
		"\26\3\26\3\26\7\26\u02c3\n\26\f\26\16\26\u02c6\13\26\3\26\3\26\3\26\3"+
		"\26\7\26\u02cc\n\26\f\26\16\26\u02cf\13\26\3\26\3\26\3\26\3\26\7\26\u02d5"+
		"\n\26\f\26\16\26\u02d8\13\26\3\27\3\27\3\30\3\30\3\31\3\31\6\31\u02e0"+
		"\n\31\r\31\16\31\u02e1\3\31\3\31\6\31\u02e6\n\31\r\31\16\31\u02e7\3\31"+
		"\3\31\3\32\3\32\5\32\u02ee\n\32\3\33\3\33\3\33\5\33\u02f3\n\33\3\33\3"+
		"\33\3\33\3\33\3\33\7\33\u02fa\n\33\f\33\16\33\u02fd\13\33\3\33\3\33\7"+
		"\33\u0301\n\33\f\33\16\33\u0304\13\33\3\33\7\33\u0307\n\33\f\33\16\33"+
		"\u030a\13\33\3\33\3\33\3\33\3\33\7\33\u0310\n\33\f\33\16\33\u0313\13\33"+
		"\3\33\5\33\u0316\n\33\5\33\u0318\n\33\3\34\3\34\3\35\3\35\3\35\3\35\3"+
		"\35\5\35\u0321\n\35\3\36\3\36\3\37\3\37\3 \3 \3!\3!\5!\u032b\n!\3\"\7"+
		"\"\u032e\n\"\f\"\16\"\u0331\13\"\3\"\5\"\u0334\n\"\3\"\3\"\3#\3#\3$\7"+
		"$\u033b\n$\f$\16$\u033e\13$\3%\7%\u0341\n%\f%\16%\u0344\13%\3%\3%\7%\u0348"+
		"\n%\f%\16%\u034b\13%\3%\3%\7%\u034f\n%\f%\16%\u0352\13%\3%\5%\u0355\n"+
		"%\3%\2\3*&\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\66"+
		"8:<>@BDFH\2\25\4\2\20\20ss\3\2\23\26\3\2>?\7\2\27\27\31\31\34\34\37\37"+
		";;\5\2\37\37##&&\4\2\31\31\34\34\4\2  $$\4\2**,,\5\2))++-.\3\2/\61\5\2"+
		"AACCEE\6\2WWZZ\\\\bd\5\2XY[[_a\3\2eg\3\2PU\3\2<=\3\2hj\3\2mp\3\2st\2\u03ca"+
		"\2K\3\2\2\2\4S\3\2\2\2\6Y\3\2\2\2\b[\3\2\2\2\np\3\2\2\2\fr\3\2\2\2\16"+
		"\u0087\3\2\2\2\20\u00b0\3\2\2\2\22\u00b2\3\2\2\2\24\u00c6\3\2\2\2\26\u00cd"+
		"\3\2\2\2\30\u00fb\3\2\2\2\32\u00fd\3\2\2\2\34\u012e\3\2\2\2\36\u0143\3"+
		"\2\2\2 \u0145\3\2\2\2\"\u014d\3\2\2\2$\u01a4\3\2\2\2&\u01a8\3\2\2\2(\u01c5"+
		"\3\2\2\2*\u01cf\3\2\2\2,\u02d9\3\2\2\2.\u02db\3\2\2\2\60\u02dd\3\2\2\2"+
		"\62\u02ed\3\2\2\2\64\u0317\3\2\2\2\66\u0319\3\2\2\28\u0320\3\2\2\2:\u0322"+
		"\3\2\2\2<\u0324\3\2\2\2>\u0326\3\2\2\2@\u032a\3\2\2\2B\u032f\3\2\2\2D"+
		"\u0337\3\2\2\2F\u033c\3\2\2\2H\u0354\3\2\2\2JL\7\3\2\2KJ\3\2\2\2KL\3\2"+
		"\2\2LN\3\2\2\2MO\5\4\3\2NM\3\2\2\2NO\3\2\2\2OP\3\2\2\2PQ\7\2\2\3Q\3\3"+
		"\2\2\2RT\5\6\4\2SR\3\2\2\2TU\3\2\2\2US\3\2\2\2UV\3\2\2\2V\5\3\2\2\2WZ"+
		"\5\b\5\2XZ\7s\2\2YW\3\2\2\2YX\3\2\2\2Z\7\3\2\2\2[\\\5*\26\2\\]\5H%\2]"+
		"\t\3\2\2\2^_\7\13\2\2_`\5F$\2`a\5\f\7\2ab\5F$\2bc\7\f\2\2cq\3\2\2\2de"+
		"\7\7\2\2ef\5F$\2fg\5\16\b\2gh\5F$\2hi\7\b\2\2iq\3\2\2\2jk\7\t\2\2kl\5"+
		"F$\2lm\5\22\n\2mn\5F$\2no\7\n\2\2oq\3\2\2\2p^\3\2\2\2pd\3\2\2\2pj\3\2"+
		"\2\2q\13\3\2\2\2r\u0083\5*\26\2su\7t\2\2ts\3\2\2\2ux\3\2\2\2vt\3\2\2\2"+
		"vw\3\2\2\2wy\3\2\2\2xv\3\2\2\2y}\t\2\2\2z|\7t\2\2{z\3\2\2\2|\177\3\2\2"+
		"\2}{\3\2\2\2}~\3\2\2\2~\u0080\3\2\2\2\177}\3\2\2\2\u0080\u0082\5*\26\2"+
		"\u0081v\3\2\2\2\u0082\u0085\3\2\2\2\u0083\u0081\3\2\2\2\u0083\u0084\3"+
		"\2\2\2\u0084\r\3\2\2\2\u0085\u0083\3\2\2\2\u0086\u0088\5\20\t\2\u0087"+
		"\u0086\3\2\2\2\u0087\u0088\3\2\2\2\u0088\u0099\3\2\2\2\u0089\u008b\7t"+
		"\2\2\u008a\u0089\3\2\2\2\u008b\u008e\3\2\2\2\u008c\u008a\3\2\2\2\u008c"+
		"\u008d\3\2\2\2\u008d\u008f\3\2\2\2\u008e\u008c\3\2\2\2\u008f\u0093\t\2"+
		"\2\2\u0090\u0092\7t\2\2\u0091\u0090\3\2\2\2\u0092\u0095\3\2\2\2\u0093"+
		"\u0091\3\2\2\2\u0093\u0094\3\2\2\2\u0094\u0096\3\2\2\2\u0095\u0093\3\2"+
		"\2\2\u0096\u0098\5\20\t\2\u0097\u008c\3\2\2\2\u0098\u009b\3\2\2\2\u0099"+
		"\u0097\3\2\2\2\u0099\u009a\3\2\2\2\u009a\u009f\3\2\2\2\u009b\u0099\3\2"+
		"\2\2\u009c\u009e\7t\2\2\u009d\u009c\3\2\2\2\u009e\u00a1\3\2\2\2\u009f"+
		"\u009d\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\u00a5\3\2\2\2\u00a1\u009f\3\2"+
		"\2\2\u00a2\u00a4\7\20\2\2\u00a3\u00a2\3\2\2\2\u00a4\u00a7\3\2\2\2\u00a5"+
		"\u00a3\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\17\3\2\2\2\u00a7\u00a5\3\2\2"+
		"\2\u00a8\u00aa\7\30\2\2\u00a9\u00a8\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa"+
		"\u00ab\3\2\2\2\u00ab\u00b1\5*\26\2\u00ac\u00ae\5*\26\2\u00ad\u00af\7\30"+
		"\2\2\u00ae\u00ad\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\u00b1\3\2\2\2\u00b0"+
		"\u00a9\3\2\2\2\u00b0\u00ac\3\2\2\2\u00b1\21\3\2\2\2\u00b2\u00c3\5\34\17"+
		"\2\u00b3\u00b5\7t\2\2\u00b4\u00b3\3\2\2\2\u00b5\u00b8\3\2\2\2\u00b6\u00b4"+
		"\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b9\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b9"+
		"\u00bd\t\2\2\2\u00ba\u00bc\7t\2\2\u00bb\u00ba\3\2\2\2\u00bc\u00bf\3\2"+
		"\2\2\u00bd\u00bb\3\2\2\2\u00bd\u00be\3\2\2\2\u00be\u00c0\3\2\2\2\u00bf"+
		"\u00bd\3\2\2\2\u00c0\u00c2\5\34\17\2\u00c1\u00b6\3\2\2\2\u00c2\u00c5\3"+
		"\2\2\2\u00c3\u00c1\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\23\3\2\2\2\u00c5"+
		"\u00c3\3\2\2\2\u00c6\u00c7\7\r\2\2\u00c7\u00c8\5\26\f\2\u00c8\u00c9\7"+
		"\16\2\2\u00c9\25\3\2\2\2\u00ca\u00cc\7\20\2\2\u00cb\u00ca\3\2\2\2\u00cc"+
		"\u00cf\3\2\2\2\u00cd\u00cb\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce\u00d3\3\2"+
		"\2\2\u00cf\u00cd\3\2\2\2\u00d0\u00d2\7t\2\2\u00d1\u00d0\3\2\2\2\u00d2"+
		"\u00d5\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00d7\3\2"+
		"\2\2\u00d5\u00d3\3\2\2\2\u00d6\u00d8\5\30\r\2\u00d7\u00d6\3\2\2\2\u00d7"+
		"\u00d8\3\2\2\2\u00d8\u00dc\3\2\2\2\u00d9\u00db\7t\2\2\u00da\u00d9\3\2"+
		"\2\2\u00db\u00de\3\2\2\2\u00dc\u00da\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd"+
		"\u00e9\3\2\2\2\u00de\u00dc\3\2\2\2\u00df\u00e3\7\20\2\2\u00e0\u00e2\7"+
		"t\2\2\u00e1\u00e0\3\2\2\2\u00e2\u00e5\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e3"+
		"\u00e4\3\2\2\2\u00e4\u00e6\3\2\2\2\u00e5\u00e3\3\2\2\2\u00e6\u00e8\5\30"+
		"\r\2\u00e7\u00df\3\2\2\2\u00e8\u00eb\3\2\2\2\u00e9\u00e7\3\2\2\2\u00e9"+
		"\u00ea\3\2\2\2\u00ea\u00ef\3\2\2\2\u00eb\u00e9\3\2\2\2\u00ec\u00ee\7t"+
		"\2\2\u00ed\u00ec\3\2\2\2\u00ee\u00f1\3\2\2\2\u00ef\u00ed\3\2\2\2\u00ef"+
		"\u00f0\3\2\2\2\u00f0\u00f5\3\2\2\2\u00f1\u00ef\3\2\2\2\u00f2\u00f4\7\20"+
		"\2\2\u00f3\u00f2\3\2\2\2\u00f4\u00f7\3\2\2\2\u00f5\u00f3\3\2\2\2\u00f5"+
		"\u00f6\3\2\2\2\u00f6\27\3\2\2\2\u00f7\u00f5\3\2\2\2\u00f8\u00fc\5B\"\2"+
		"\u00f9\u00fc\5\n\6\2\u00fa\u00fc\5\34\17\2\u00fb\u00f8\3\2\2\2\u00fb\u00f9"+
		"\3\2\2\2\u00fb\u00fa\3\2\2\2\u00fc\31\3\2\2\2\u00fd\u00fe\t\3\2\2\u00fe"+
		"\33\3\2\2\2\u00ff\u0100\5\32\16\2\u0100\u0101\5\36\20\2\u0101\u012f\3"+
		"\2\2\2\u0102\u0103\5\36\20\2\u0103\u0107\5\32\16\2\u0104\u0106\7t\2\2"+
		"\u0105\u0104\3\2\2\2\u0106\u0109\3\2\2\2\u0107\u0105\3\2\2\2\u0107\u0108"+
		"\3\2\2\2\u0108\u010a\3\2\2\2\u0109\u0107\3\2\2\2\u010a\u010b\5*\26\2\u010b"+
		"\u012f\3\2\2\2\u010c\u0110\7\t\2\2\u010d\u010f\7t\2\2\u010e\u010d\3\2"+
		"\2\2\u010f\u0112\3\2\2\2\u0110\u010e\3\2\2\2\u0110\u0111\3\2\2\2\u0111"+
		"\u0113\3\2\2\2\u0112\u0110\3\2\2\2\u0113\u0117\5*\26\2\u0114\u0116\7t"+
		"\2\2\u0115\u0114\3\2\2\2\u0116\u0119\3\2\2\2\u0117\u0115\3\2\2\2\u0117"+
		"\u0118\3\2\2\2\u0118\u011a\3\2\2\2\u0119\u0117\3\2\2\2\u011a\u011b\7\n"+
		"\2\2\u011b\u011f\5\32\16\2\u011c\u011e\7t\2\2\u011d\u011c\3\2\2\2\u011e"+
		"\u0121\3\2\2\2\u011f\u011d\3\2\2\2\u011f\u0120\3\2\2\2\u0120\u0122\3\2"+
		"\2\2\u0121\u011f\3\2\2\2\u0122\u0123\5*\26\2\u0123\u012f\3\2\2\2\u0124"+
		"\u0125\5\36\20\2\u0125\u0129\5\32\16\2\u0126\u0128\7t\2\2\u0127\u0126"+
		"\3\2\2\2\u0128\u012b\3\2\2\2\u0129\u0127\3\2\2\2\u0129\u012a\3\2\2\2\u012a"+
		"\u012c\3\2\2\2\u012b\u0129\3\2\2\2\u012c\u012d\5\60\31\2\u012d\u012f\3"+
		"\2\2\2\u012e\u00ff\3\2\2\2\u012e\u0102\3\2\2\2\u012e\u010c\3\2\2\2\u012e"+
		"\u0124\3\2\2\2\u012f\35\3\2\2\2\u0130\u0144\5@!\2\u0131\u0144\7r\2\2\u0132"+
		"\u0144\5> \2\u0133\u0137\7\7\2\2\u0134\u0136\7t\2\2\u0135\u0134\3\2\2"+
		"\2\u0136\u0139\3\2\2\2\u0137\u0135\3\2\2\2\u0137\u0138\3\2\2\2\u0138\u013a"+
		"\3\2\2\2\u0139\u0137\3\2\2\2\u013a\u013e\5*\26\2\u013b\u013d\7t\2\2\u013c"+
		"\u013b\3\2\2\2\u013d\u0140\3\2\2\2\u013e\u013c\3\2\2\2\u013e\u013f\3\2"+
		"\2\2\u013f\u0141\3\2\2\2\u0140\u013e\3\2\2\2\u0141\u0142\7\b\2\2\u0142"+
		"\u0144\3\2\2\2\u0143\u0130\3\2\2\2\u0143\u0131\3\2\2\2\u0143\u0132\3\2"+
		"\2\2\u0143\u0133\3\2\2\2\u0144\37\3\2\2\2\u0145\u0147\5\"\22\2\u0146\u0148"+
		"\5$\23\2\u0147\u0146\3\2\2\2\u0148\u0149\3\2\2\2\u0149\u0147\3\2\2\2\u0149"+
		"\u014a\3\2\2\2\u014a!\3\2\2\2\u014b\u014e\5\n\6\2\u014c\u014e\5B\"\2\u014d"+
		"\u014b\3\2\2\2\u014d\u014c\3\2\2\2\u014e#\3\2\2\2\u014f\u01a5\78\2\2\u0150"+
		"\u0154\7\7\2\2\u0151\u0153\7t\2\2\u0152\u0151\3\2\2\2\u0153\u0156\3\2"+
		"\2\2\u0154\u0152\3\2\2\2\u0154\u0155\3\2\2\2\u0155\u0157\3\2\2\2\u0156"+
		"\u0154\3\2\2\2\u0157\u01a5\7\b\2\2\u0158\u015c\7\7\2\2\u0159\u015b\7t"+
		"\2\2\u015a\u0159\3\2\2\2\u015b\u015e\3\2\2\2\u015c\u015a\3\2\2\2\u015c"+
		"\u015d\3\2\2\2\u015d\u015f\3\2\2\2\u015e\u015c\3\2\2\2\u015f\u0163\7\21"+
		"\2\2\u0160\u0162\7t\2\2\u0161\u0160\3\2\2\2\u0162\u0165\3\2\2\2\u0163"+
		"\u0161\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0166\3\2\2\2\u0165\u0163\3\2"+
		"\2\2\u0166\u01a5\7\b\2\2\u0167\u016b\7\7\2\2\u0168\u016a\7t\2\2\u0169"+
		"\u0168\3\2\2\2\u016a\u016d\3\2\2\2\u016b\u0169\3\2\2\2\u016b\u016c\3\2"+
		"\2\2\u016c\u016e\3\2\2\2\u016d\u016b\3\2\2\2\u016e\u0172\5&\24\2\u016f"+
		"\u0171\7t\2\2\u0170\u016f\3\2\2\2\u0171\u0174\3\2\2\2\u0172\u0170\3\2"+
		"\2\2\u0172\u0173\3\2\2\2\u0173\u0175\3\2\2\2\u0174\u0172\3\2\2\2\u0175"+
		"\u0176\7\b\2\2\u0176\u01a5\3\2\2\2\u0177\u017b\7\t\2\2\u0178\u017a\7t"+
		"\2\2\u0179\u0178\3\2\2\2\u017a\u017d\3\2\2\2\u017b\u0179\3\2\2\2\u017b"+
		"\u017c\3\2\2\2\u017c\u017e\3\2\2\2\u017d\u017b\3\2\2\2\u017e\u01a5\7\b"+
		"\2\2\u017f\u0183\7\t\2\2\u0180\u0182\7t\2\2\u0181\u0180\3\2\2\2\u0182"+
		"\u0185\3\2\2\2\u0183\u0181\3\2\2\2\u0183\u0184\3\2\2\2\u0184\u0186\3\2"+
		"\2\2\u0185\u0183\3\2\2\2\u0186\u018a\t\4\2\2\u0187\u0189\7t\2\2\u0188"+
		"\u0187\3\2\2\2\u0189\u018c\3\2\2\2\u018a\u0188\3\2\2\2\u018a\u018b\3\2"+
		"\2\2\u018b\u018d\3\2\2\2\u018c\u018a\3\2\2\2\u018d\u01a5\7\n\2\2\u018e"+
		"\u0192\7\t\2\2\u018f\u0191\7t\2\2\u0190\u018f\3\2\2\2\u0191\u0194\3\2"+
		"\2\2\u0192\u0190\3\2\2\2\u0192\u0193\3\2\2\2\u0193\u0195\3\2\2\2\u0194"+
		"\u0192\3\2\2\2\u0195\u0199\5&\24\2\u0196\u0198\7t\2\2\u0197\u0196\3\2"+
		"\2\2\u0198\u019b\3\2\2\2\u0199\u0197\3\2\2\2\u0199\u019a\3\2\2\2\u019a"+
		"\u019c\3\2\2\2\u019b\u0199\3\2\2\2\u019c\u019d\7\b\2\2\u019d\u01a5\3\2"+
		"\2\2\u019e\u01a0\7t\2\2\u019f\u019e\3\2\2\2\u01a0\u01a1\3\2\2\2\u01a1"+
		"\u019f\3\2\2\2\u01a1\u01a2\3\2\2\2\u01a2\u01a3\3\2\2\2\u01a3\u01a5\5&"+
		"\24\2\u01a4\u014f\3\2\2\2\u01a4\u0150\3\2\2\2\u01a4\u0158\3\2\2\2\u01a4"+
		"\u0167\3\2\2\2\u01a4\u0177\3\2\2\2\u01a4\u017f\3\2\2\2\u01a4\u018e\3\2"+
		"\2\2\u01a4\u019f\3\2\2\2\u01a5%\3\2\2\2\u01a6\u01a9\5*\26\2\u01a7\u01a9"+
		"\5\34\17\2\u01a8\u01a6\3\2\2\2\u01a8\u01a7\3\2\2\2\u01a9\u01ad\3\2\2\2"+
		"\u01aa\u01ac\7t\2\2\u01ab\u01aa\3\2\2\2\u01ac\u01af\3\2\2\2\u01ad\u01ab"+
		"\3\2\2\2\u01ad\u01ae\3\2\2\2\u01ae\u01bd\3\2\2\2\u01af\u01ad\3\2\2\2\u01b0"+
		"\u01b4\7\20\2\2\u01b1\u01b3\7t\2\2\u01b2\u01b1\3\2\2\2\u01b3\u01b6\3\2"+
		"\2\2\u01b4\u01b2\3\2\2\2\u01b4\u01b5\3\2\2\2\u01b5\u01b9\3\2\2\2\u01b6"+
		"\u01b4\3\2\2\2\u01b7\u01ba\5*\26\2\u01b8\u01ba\5\34\17\2\u01b9\u01b7\3"+
		"\2\2\2\u01b9\u01b8\3\2\2\2\u01ba\u01bc\3\2\2\2\u01bb\u01b0\3\2\2\2\u01bc"+
		"\u01bf\3\2\2\2\u01bd\u01bb\3\2\2\2\u01bd\u01be\3\2\2\2\u01be\'\3\2\2\2"+
		"\u01bf\u01bd\3\2\2\2\u01c0\u01c6\58\35\2\u01c1\u01c6\5:\36\2\u01c2\u01c6"+
		"\5 \21\2\u01c3\u01c6\5B\"\2\u01c4\u01c6\5\n\6\2\u01c5\u01c0\3\2\2\2\u01c5"+
		"\u01c1\3\2\2\2\u01c5\u01c2\3\2\2\2\u01c5\u01c3\3\2\2\2\u01c5\u01c4\3\2"+
		"\2\2\u01c6)\3\2\2\2\u01c7\u01c8\b\26\1\2\u01c8\u01d0\5(\25\2\u01c9\u01ca"+
		"\5(\25\2\u01ca\u01cb\7\27\2\2\u01cb\u01d0\3\2\2\2\u01cc\u01cd\t\5\2\2"+
		"\u01cd\u01d0\5(\25\2\u01ce\u01d0\5\60\31\2\u01cf\u01c7\3\2\2\2\u01cf\u01c9"+
		"\3\2\2\2\u01cf\u01cc\3\2\2\2\u01cf\u01ce\3\2\2\2\u01d0\u02d6\3\2\2\2\u01d1"+
		"\u01d2\f\30\2\2\u01d2\u01d3\7\"\2\2\u01d3\u02d5\5*\26\31\u01d4\u01d6\f"+
		"\26\2\2\u01d5\u01d7\7t\2\2\u01d6\u01d5\3\2\2\2\u01d7\u01d8\3\2\2\2\u01d8"+
		"\u01d6\3\2\2\2\u01d8\u01d9\3\2\2\2\u01d9\u01da\3\2\2\2\u01da\u01dc\t\6"+
		"\2\2\u01db\u01dd\7t\2\2\u01dc\u01db\3\2\2\2\u01dd\u01de\3\2\2\2\u01de"+
		"\u01dc\3\2\2\2\u01de\u01df\3\2\2\2\u01df\u01e0\3\2\2\2\u01e0\u02d5\5*"+
		"\26\27\u01e1\u01e3\f\25\2\2\u01e2\u01e4\7t\2\2\u01e3\u01e2\3\2\2\2\u01e4"+
		"\u01e5\3\2\2\2\u01e5\u01e3\3\2\2\2\u01e5\u01e6\3\2\2\2\u01e6\u01e7\3\2"+
		"\2\2\u01e7\u01e9\t\7\2\2\u01e8\u01ea\7t\2\2\u01e9\u01e8\3\2\2\2\u01ea"+
		"\u01eb\3\2\2\2\u01eb\u01e9\3\2\2\2\u01eb\u01ec\3\2\2\2\u01ec\u01ed\3\2"+
		"\2\2\u01ed\u02d5\5*\26\26\u01ee\u01f2\f\23\2\2\u01ef\u01f1\7t\2\2\u01f0"+
		"\u01ef\3\2\2\2\u01f1\u01f4\3\2\2\2\u01f2\u01f0\3\2\2\2\u01f2\u01f3\3\2"+
		"\2\2\u01f3\u01f5\3\2\2\2\u01f4\u01f2\3\2\2\2\u01f5\u01f9\t\b\2\2\u01f6"+
		"\u01f8\7t\2\2\u01f7\u01f6\3\2\2\2\u01f8\u01fb\3\2\2\2\u01f9\u01f7\3\2"+
		"\2\2\u01f9\u01fa\3\2\2\2\u01fa\u01fc\3\2\2\2\u01fb\u01f9\3\2\2\2\u01fc"+
		"\u02d5\5*\26\23\u01fd\u0201\f\22\2\2\u01fe\u0200\7t\2\2\u01ff\u01fe\3"+
		"\2\2\2\u0200\u0203\3\2\2\2\u0201\u01ff\3\2\2\2\u0201\u0202\3\2\2\2\u0202"+
		"\u0204\3\2\2\2\u0203\u0201\3\2\2\2\u0204\u0208\t\t\2\2\u0205\u0207\7t"+
		"\2\2\u0206\u0205\3\2\2\2\u0207\u020a\3\2\2\2\u0208\u0206\3\2\2\2\u0208"+
		"\u0209\3\2\2\2\u0209\u020b\3\2\2\2\u020a\u0208\3\2\2\2\u020b\u02d5\5*"+
		"\26\23\u020c\u0210\f\21\2\2\u020d\u020f\7t\2\2\u020e\u020d\3\2\2\2\u020f"+
		"\u0212\3\2\2\2\u0210\u020e\3\2\2\2\u0210\u0211\3\2\2\2\u0211\u0213\3\2"+
		"\2\2\u0212\u0210\3\2\2\2\u0213\u0217\7\67\2\2\u0214\u0216\7t\2\2\u0215"+
		"\u0214\3\2\2\2\u0216\u0219\3\2\2\2\u0217\u0215\3\2\2\2\u0217\u0218\3\2"+
		"\2\2\u0218\u021a\3\2\2\2\u0219\u0217\3\2\2\2\u021a\u02d5\5*\26\22\u021b"+
		"\u021f\f\20\2\2\u021c\u021e\7t\2\2\u021d\u021c\3\2\2\2\u021e\u0221\3\2"+
		"\2\2\u021f\u021d\3\2\2\2\u021f\u0220\3\2\2\2\u0220\u0222\3\2\2\2\u0221"+
		"\u021f\3\2\2\2\u0222\u0226\7:\2\2\u0223\u0225\7t\2\2\u0224\u0223\3\2\2"+
		"\2\u0225\u0228\3\2\2\2\u0226\u0224\3\2\2\2\u0226\u0227\3\2\2\2\u0227\u0229"+
		"\3\2\2\2\u0228\u0226\3\2\2\2\u0229\u02d5\5*\26\21\u022a\u022e\f\17\2\2"+
		"\u022b\u022d\7t\2\2\u022c\u022b\3\2\2\2\u022d\u0230\3\2\2\2\u022e\u022c"+
		"\3\2\2\2\u022e\u022f\3\2\2\2\u022f\u0231\3\2\2\2\u0230\u022e\3\2\2\2\u0231"+
		"\u0235\7\64\2\2\u0232\u0234\7t\2\2\u0233\u0232\3\2\2\2\u0234\u0237\3\2"+
		"\2\2\u0235\u0233\3\2\2\2\u0235\u0236\3\2\2\2\u0236\u0238\3\2\2\2\u0237"+
		"\u0235\3\2\2\2\u0238\u02d5\5*\26\20\u0239\u023d\f\16\2\2\u023a\u023c\7"+
		"t\2\2\u023b\u023a\3\2\2\2\u023c\u023f\3\2\2\2\u023d\u023b\3\2\2\2\u023d"+
		"\u023e\3\2\2\2\u023e\u0240\3\2\2\2\u023f\u023d\3\2\2\2\u0240\u0244\t\n"+
		"\2\2\u0241\u0243\7t\2\2\u0242\u0241\3\2\2\2\u0243\u0246\3\2\2\2\u0244"+
		"\u0242\3\2\2\2\u0244\u0245\3\2\2\2\u0245\u0247\3\2\2\2\u0246\u0244\3\2"+
		"\2\2\u0247\u02d5\5*\26\17\u0248\u024c\f\r\2\2\u0249\u024b\7t\2\2\u024a"+
		"\u0249\3\2\2\2\u024b\u024e\3\2\2\2\u024c\u024a\3\2\2\2\u024c\u024d\3\2"+
		"\2\2\u024d\u024f\3\2\2\2\u024e\u024c\3\2\2\2\u024f\u0253\7\66\2\2\u0250"+
		"\u0252\7t\2\2\u0251\u0250\3\2\2\2\u0252\u0255\3\2\2\2\u0253\u0251\3\2"+
		"\2\2\u0253\u0254\3\2\2\2\u0254\u0256\3\2\2\2\u0255\u0253\3\2\2\2\u0256"+
		"\u02d5\5*\26\16\u0257\u025b\f\f\2\2\u0258\u025a\7t\2\2\u0259\u0258\3\2"+
		"\2\2\u025a\u025d\3\2\2\2\u025b\u0259\3\2\2\2\u025b\u025c\3\2\2\2\u025c"+
		"\u025e\3\2\2\2\u025d\u025b\3\2\2\2\u025e\u0262\79\2\2\u025f\u0261\7t\2"+
		"\2\u0260\u025f\3\2\2\2\u0261\u0264\3\2\2\2\u0262\u0260\3\2\2\2\u0262\u0263"+
		"\3\2\2\2\u0263\u0265\3\2\2\2\u0264\u0262\3\2\2\2\u0265\u02d5\5*\26\r\u0266"+
		"\u026a\f\13\2\2\u0267\u0269\7t\2\2\u0268\u0267\3\2\2\2\u0269\u026c\3\2"+
		"\2\2\u026a\u0268\3\2\2\2\u026a\u026b\3\2\2\2\u026b\u026d\3\2\2\2\u026c"+
		"\u026a\3\2\2\2\u026d\u0271\7\63\2\2\u026e\u0270\7t\2\2\u026f\u026e\3\2"+
		"\2\2\u0270\u0273\3\2\2\2\u0271\u026f\3\2\2\2\u0271\u0272\3\2\2\2\u0272"+
		"\u0274\3\2\2\2\u0273\u0271\3\2\2\2\u0274\u02d5\5*\26\f\u0275\u0279\f\n"+
		"\2\2\u0276\u0278\7t\2\2\u0277\u0276\3\2\2\2\u0278\u027b\3\2\2\2\u0279"+
		"\u0277\3\2\2\2\u0279\u027a\3\2\2\2\u027a\u027c\3\2\2\2\u027b\u0279\3\2"+
		"\2\2\u027c\u0280\t\13\2\2\u027d\u027f\7t\2\2\u027e\u027d\3\2\2\2\u027f"+
		"\u0282\3\2\2\2\u0280\u027e\3\2\2\2\u0280\u0281\3\2\2\2\u0281\u0283\3\2"+
		"\2\2\u0282\u0280\3\2\2\2\u0283\u02d5\5*\26\13\u0284\u0288\f\t\2\2\u0285"+
		"\u0287\7t\2\2\u0286\u0285\3\2\2\2\u0287\u028a\3\2\2\2\u0288\u0286\3\2"+
		"\2\2\u0288\u0289\3\2\2\2\u0289\u028b\3\2\2\2\u028a\u0288\3\2\2\2\u028b"+
		"\u028f\7@\2\2\u028c\u028e\7t\2\2\u028d\u028c\3\2\2\2\u028e\u0291\3\2\2"+
		"\2\u028f\u028d\3\2\2\2\u028f\u0290\3\2\2\2\u0290\u0292\3\2\2\2\u0291\u028f"+
		"\3\2\2\2\u0292\u02d5\5*\26\n\u0293\u0294\f\b\2\2\u0294\u0295\5F$\2\u0295"+
		"\u0296\t\f\2\2\u0296\u0297\5F$\2\u0297\u0298\5*\26\t\u0298\u02d5\3\2\2"+
		"\2\u0299\u029d\f\7\2\2\u029a\u029c\7t\2\2\u029b\u029a\3\2\2\2\u029c\u029f"+
		"\3\2\2\2\u029d\u029b\3\2\2\2\u029d\u029e\3\2\2\2\u029e\u02a0\3\2\2\2\u029f"+
		"\u029d\3\2\2\2\u02a0\u02a4\5.\30\2\u02a1\u02a3\7t\2\2\u02a2\u02a1\3\2"+
		"\2\2\u02a3\u02a6\3\2\2\2\u02a4\u02a2\3\2\2\2\u02a4\u02a5\3\2\2\2\u02a5"+
		"\u02a7\3\2\2\2\u02a6\u02a4\3\2\2\2\u02a7\u02a8\5*\26\b\u02a8\u02d5\3\2"+
		"\2\2\u02a9\u02ad\f\6\2\2\u02aa\u02ac\7t\2\2\u02ab\u02aa\3\2\2\2\u02ac"+
		"\u02af\3\2\2\2\u02ad\u02ab\3\2\2\2\u02ad\u02ae\3\2\2\2\u02ae\u02b0\3\2"+
		"\2\2\u02af\u02ad\3\2\2\2\u02b0\u02b4\5,\27\2\u02b1\u02b3\7t\2\2\u02b2"+
		"\u02b1\3\2\2\2\u02b3\u02b6\3\2\2\2\u02b4\u02b2\3\2\2\2\u02b4\u02b5\3\2"+
		"\2\2\u02b5\u02b7\3\2\2\2\u02b6\u02b4\3\2\2\2\u02b7\u02b8\5*\26\6\u02b8"+
		"\u02d5\3\2\2\2\u02b9\u02bd\f\4\2\2\u02ba\u02bc\7t\2\2\u02bb\u02ba\3\2"+
		"\2\2\u02bc\u02bf\3\2\2\2\u02bd\u02bb\3\2\2\2\u02bd\u02be\3\2\2\2\u02be"+
		"\u02c0\3\2\2\2\u02bf\u02bd\3\2\2\2\u02c0\u02c4\5\66\34\2\u02c1\u02c3\7"+
		"t\2\2\u02c2\u02c1\3\2\2\2\u02c3\u02c6\3\2\2\2\u02c4\u02c2\3\2\2\2\u02c4"+
		"\u02c5\3\2\2\2\u02c5\u02c7\3\2\2\2\u02c6\u02c4\3\2\2\2\u02c7\u02c8\5*"+
		"\26\4\u02c8\u02d5\3\2\2\2\u02c9\u02cd\f\3\2\2\u02ca\u02cc\7t\2\2\u02cb"+
		"\u02ca\3\2\2\2\u02cc\u02cf\3\2\2\2\u02cd\u02cb\3\2\2\2\u02cd\u02ce\3\2"+
		"\2\2\u02ce\u02d0\3\2\2\2\u02cf\u02cd\3\2\2\2\u02d0\u02d1\7V\2\2\u02d1"+
		"\u02d2\5F$\2\u02d2\u02d3\5\22\n\2\u02d3\u02d5\3\2\2\2\u02d4\u01d1\3\2"+
		"\2\2\u02d4\u01d4\3\2\2\2\u02d4\u01e1\3\2\2\2\u02d4\u01ee\3\2\2\2\u02d4"+
		"\u01fd\3\2\2\2\u02d4\u020c\3\2\2\2\u02d4\u021b\3\2\2\2\u02d4\u022a\3\2"+
		"\2\2\u02d4\u0239\3\2\2\2\u02d4\u0248\3\2\2\2\u02d4\u0257\3\2\2\2\u02d4"+
		"\u0266\3\2\2\2\u02d4\u0275\3\2\2\2\u02d4\u0284\3\2\2\2\u02d4\u0293\3\2"+
		"\2\2\u02d4\u0299\3\2\2\2\u02d4\u02a9\3\2\2\2\u02d4\u02b9\3\2\2\2\u02d4"+
		"\u02c9\3\2\2\2\u02d5\u02d8\3\2\2\2\u02d6\u02d4\3\2\2\2\u02d6\u02d7\3\2"+
		"\2\2\u02d7+\3\2\2\2\u02d8\u02d6\3\2\2\2\u02d9\u02da\t\r\2\2\u02da-\3\2"+
		"\2\2\u02db\u02dc\t\16\2\2\u02dc/\3\2\2\2\u02dd\u02df\5\62\32\2\u02de\u02e0"+
		"\7t\2\2\u02df\u02de\3\2\2\2\u02e0\u02e1\3\2\2\2\u02e1\u02df\3\2\2\2\u02e1"+
		"\u02e2\3\2\2\2\u02e2\u02e3\3\2\2\2\u02e3\u02e5\t\17\2\2\u02e4\u02e6\7"+
		"t\2\2\u02e5\u02e4\3\2\2\2\u02e6\u02e7\3\2\2\2\u02e7\u02e5\3\2\2\2\u02e7"+
		"\u02e8\3\2\2\2\u02e8\u02e9\3\2\2\2\u02e9\u02ea\5\64\33\2\u02ea\61\3\2"+
		"\2\2\u02eb\u02ee\5\24\13\2\u02ec\u02ee\5\26\f\2\u02ed\u02eb\3\2\2\2\u02ed"+
		"\u02ec\3\2\2\2\u02ee\63\3\2\2\2\u02ef\u02f0\7\13\2\2\u02f0\u02f2\5F$\2"+
		"\u02f1\u02f3\5\4\3\2\u02f2\u02f1\3\2\2\2\u02f2\u02f3\3\2\2\2\u02f3\u02f4"+
		"\3\2\2\2\u02f4\u02f5\5F$\2\u02f5\u02f6\7\f\2\2\u02f6\u0318\3\2\2\2\u02f7"+
		"\u02fb\5*\26\2\u02f8\u02fa\7t\2\2\u02f9\u02f8\3\2\2\2\u02fa\u02fd\3\2"+
		"\2\2\u02fb\u02f9\3\2\2\2\u02fb\u02fc\3\2\2\2\u02fc\u0308\3\2\2\2\u02fd"+
		"\u02fb\3\2\2\2\u02fe\u0302\7\20\2\2\u02ff\u0301\7t\2\2\u0300\u02ff\3\2"+
		"\2\2\u0301\u0304\3\2\2\2\u0302\u0300\3\2\2\2\u0302\u0303\3\2\2\2\u0303"+
		"\u0305\3\2\2\2\u0304\u0302\3\2\2\2\u0305\u0307\5*\26\2\u0306\u02fe\3\2"+
		"\2\2\u0307\u030a\3\2\2\2\u0308\u0306\3\2\2\2\u0308\u0309\3\2\2\2\u0309"+
		"\u030b\3\2\2\2\u030a\u0308\3\2\2\2\u030b\u030c\5F$\2\u030c\u0318\3\2\2"+
		"\2\u030d\u0316\5H%\2\u030e\u0310\7t\2\2\u030f\u030e\3\2\2\2\u0310\u0313"+
		"\3\2\2\2\u0311\u030f\3\2\2\2\u0311\u0312\3\2\2\2\u0312\u0314\3\2\2\2\u0313"+
		"\u0311\3\2\2\2\u0314\u0316\7s\2\2\u0315\u030d\3\2\2\2\u0315\u0311\3\2"+
		"\2\2\u0316\u0318\3\2\2\2\u0317\u02ef\3\2\2\2\u0317\u02f7\3\2\2\2\u0317"+
		"\u0315\3\2\2\2\u0318\65\3\2\2\2\u0319\u031a\t\20\2\2\u031a\67\3\2\2\2"+
		"\u031b\u0321\7k\2\2\u031c\u0321\7l\2\2\u031d\u0321\7r\2\2\u031e\u0321"+
		"\5<\37\2\u031f\u0321\5> \2\u0320\u031b\3\2\2\2\u0320\u031c\3\2\2\2\u0320"+
		"\u031d\3\2\2\2\u0320\u031e\3\2\2\2\u0320\u031f\3\2\2\2\u03219\3\2\2\2"+
		"\u0322\u0323\t\21\2\2\u0323;\3\2\2\2\u0324\u0325\t\22\2\2\u0325=\3\2\2"+
		"\2\u0326\u0327\t\23\2\2\u0327?\3\2\2\2\u0328\u032b\5B\"\2\u0329\u032b"+
		"\5D#\2\u032a\u0328\3\2\2\2\u032a\u0329\3\2\2\2\u032bA\3\2\2\2\u032c\u032e"+
		"\7t\2\2\u032d\u032c\3\2\2\2\u032e\u0331\3\2\2\2\u032f\u032d\3\2\2\2\u032f"+
		"\u0330\3\2\2\2\u0330\u0333\3\2\2\2\u0331\u032f\3\2\2\2\u0332\u0334\7\""+
		"\2\2\u0333\u0332\3\2\2\2\u0333\u0334\3\2\2\2\u0334\u0335\3\2\2\2\u0335"+
		"\u0336\7q\2\2\u0336C\3\2\2\2\u0337\u0338\7l\2\2\u0338E\3\2\2\2\u0339\u033b"+
		"\t\24\2\2\u033a\u0339\3\2\2\2\u033b\u033e\3\2\2\2\u033c\u033a\3\2\2\2"+
		"\u033c\u033d\3\2\2\2\u033dG\3\2\2\2\u033e\u033c\3\2\2\2\u033f\u0341\7"+
		"t\2\2\u0340\u033f\3\2\2\2\u0341\u0344\3\2\2\2\u0342\u0340\3\2\2\2\u0342"+
		"\u0343\3\2\2\2\u0343\u0345\3\2\2\2\u0344\u0342\3\2\2\2\u0345\u0355\7\17"+
		"\2\2\u0346\u0348\7t\2\2\u0347\u0346\3\2\2\2\u0348\u034b\3\2\2\2\u0349"+
		"\u0347\3\2\2\2\u0349\u034a\3\2\2\2\u034a\u034c\3\2\2\2\u034b\u0349\3\2"+
		"\2\2\u034c\u0355\7\2\2\3\u034d\u034f\7t\2\2\u034e\u034d\3\2\2\2\u034f"+
		"\u0352\3\2\2\2\u0350\u034e\3\2\2\2\u0350\u0351\3\2\2\2\u0351\u0353\3\2"+
		"\2\2\u0352\u0350\3\2\2\2\u0353\u0355\7s\2\2\u0354\u0342\3\2\2\2\u0354"+
		"\u0349\3\2\2\2\u0354\u0350\3\2\2\2\u0355I\3\2\2\2sKNUYpv}\u0083\u0087"+
		"\u008c\u0093\u0099\u009f\u00a5\u00a9\u00ae\u00b0\u00b6\u00bd\u00c3\u00cd"+
		"\u00d3\u00d7\u00dc\u00e3\u00e9\u00ef\u00f5\u00fb\u0107\u0110\u0117\u011f"+
		"\u0129\u012e\u0137\u013e\u0143\u0149\u014d\u0154\u015c\u0163\u016b\u0172"+
		"\u017b\u0183\u018a\u0192\u0199\u01a1\u01a4\u01a8\u01ad\u01b4\u01b9\u01bd"+
		"\u01c5\u01cf\u01d8\u01de\u01e5\u01eb\u01f2\u01f9\u0201\u0208\u0210\u0217"+
		"\u021f\u0226\u022e\u0235\u023d\u0244\u024c\u0253\u025b\u0262\u026a\u0271"+
		"\u0279\u0280\u0288\u028f\u029d\u02a4\u02ad\u02b4\u02bd\u02c4\u02cd\u02d4"+
		"\u02d6\u02e1\u02e7\u02ed\u02f2\u02fb\u0302\u0308\u0311\u0315\u0317\u0320"+
		"\u032a\u032f\u0333\u033c\u0342\u0349\u0350\u0354";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}