# Quarrel Grammar Test 0010

Here is the first test file of Quarrel

## Hello World

```
$ echo  >test.qvr '|-| <- "Hello world!"'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (ioLiteral |-|)))))
|         (singleExpression \s)
|         (singleExpression (leftTransform <-))
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal "Hello world!"))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

This prints the text 'Hello world!' to `STDOUT`

## Comments

```
$ echo  >test.qvr '# comment to end of line'
$ echo >>test.qvr '123'
$ echo >>test.qvr '# another comment'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements (sourceElement \n))
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (nonBinaryExpression (literal (numericLiteral 123)))))
|       (expressionStatement (eos \n))
|   (sourceElements (sourceElement \n))
| (program <EOF>)
| 
```

### Multiline comments

```
$ echo  >test.qvr '['
$ echo >>test.qvr '    1, 2,'
$ echo >>test.qvr '###'
$ echo >>test.qvr '    3, 4,'
$ echo >>test.qvr '###'
$ echo >>test.qvr ']'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (nonBinaryExpression
|             (compoundLiteral [)
|             (compoundLiteral (gap \n        ))
|             (compoundLiteral
|               (listElements
|                 (listElement
|                   (singleExpression
|                     (nonBinaryExpression (literal (numericLiteral 1))))
|               (listElements ,)
|               (listElements \s)
|               (listElements
|                 (listElement
|                   (singleExpression
|                     (nonBinaryExpression (literal (numericLiteral 2))))
|               (listElements ,)
|             (compoundLiteral (gap \n \n))
|             (compoundLiteral ])
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```


### Bracketed Comments

```
$ echo  >test.qvr '[1, #[bracketed]# 2, #[comments]# 3]'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (nonBinaryExpression
|             (compoundLiteral [)
|             (compoundLiteral gap)
|             (compoundLiteral
|               (listElements
|                 (listElement
|                   (singleExpression
|                     (nonBinaryExpression (literal (numericLiteral 1))))
|               (listElements ,)
|               (listElements
|                 (listElement
|                   (singleExpression
|                     (nonBinaryExpression (literal (numericLiteral 2))))
|               (listElements ,)
|               (listElements
|                 (listElement
|                   (singleExpression
|                     (nonBinaryExpression (literal (numericLiteral 3))))
|             (compoundLiteral gap)
|             (compoundLiteral ])
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

