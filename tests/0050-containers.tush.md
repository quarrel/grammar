# Quarrel Grammar Test 0050

Here are the tests for basic literals in Quarrel

## Assignment Expressions

```
$ echo  >test.qvr 'foo .= 123'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container foo))))
|         (singleExpression \s)
|         (singleExpression (assignmentOperator .=))
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 123))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'bar .= "hello"'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container bar))))
|         (singleExpression \s)
|         (singleExpression (assignmentOperator .=))
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal "hello"))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'baaz := bar'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container baaz))))
|         (singleExpression \s)
|         (singleExpression (assignmentOperator :=))
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container bar))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'quux := ()'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container quux))))
|         (singleExpression \s)
|         (singleExpression (assignmentOperator :=))
|         (singleExpression \s)
|         (singleExpression (singleExpression (nonBinaryExpression (literal ()))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'foo .= bar .= baaz := quux'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container foo))))
|         (singleExpression \s)
|         (singleExpression (assignmentOperator .=))
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression
|             (singleExpression (nonBinaryExpression (container bar))))
|           (singleExpression \s)
|           (singleExpression (assignmentOperator .=))
|           (singleExpression \s)
|           (singleExpression
|             (singleExpression
|               (singleExpression (nonBinaryExpression (container baaz))))
|             (singleExpression \s)
|             (singleExpression (assignmentOperator :=))
|             (singleExpression \s)
|             (singleExpression
|               (singleExpression (nonBinaryExpression (container quux))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

## Spaced Containers

```
$ echo  >test.qvr ''\'long container name\' .= 123''
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression
|             (nonBinaryExpression (container 'long container name')))
|         (singleExpression \s)
|         (singleExpression (assignmentOperator .=))
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 123))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

