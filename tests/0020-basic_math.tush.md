# Quarrel Grammar Test 0020

Here are the tests for basic math in Quarrel

## Operators

### Addition

```
$ echo  >test.qvr '1 + 1'
$ cd .. && pp-s-exp  ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 1)))))
|         (singleExpression \s)
|         (singleExpression +)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 1)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

You can add two number literals together

```
$ echo  >test.qvr 'num + (100)'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container num))))
|         (singleExpression \s)
|         (singleExpression +)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression
|             (nonBinaryExpression
|               (compoundLiteral ()
|               (compoundLiteral gap)
|               (compoundLiteral
|                 (sequenceElements
|                   (singleExpression
|                     (nonBinaryExpression (literal (numericLiteral 100))))
|               (compoundLiteral gap)
|               (compoundLiteral ))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

You can add expressions and containers

### Subtraction

```
$ echo  >test.qvr '1 - 1'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 1)))))
|         (singleExpression \s)
|         (singleExpression -)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 1)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

You can subtract two number literals

```
$ echo  >test.qvr 'num - (1)'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container num))))
|         (singleExpression \s)
|         (singleExpression -)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression
|             (nonBinaryExpression
|               (compoundLiteral ()
|               (compoundLiteral gap)
|               (compoundLiteral
|                 (sequenceElements
|                   (singleExpression
|                     (nonBinaryExpression (literal (numericLiteral 1))))
|               (compoundLiteral gap)
|               (compoundLiteral ))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

You can subtract containers and expressions

```
$ echo  >test.qvr '10 - -1'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 10))))
|         (singleExpression \s)
|         (singleExpression -)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression -)
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 1))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

You can subtract negative numbers


### Multiplication

```
$ echo  >test.qvr '1 * 1'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 1)))))
|         (singleExpression \s)
|         (singleExpression *)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 1)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

You can multiply two number literals together

```
$ echo  >test.qvr 'num * (100)'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container num))))
|         (singleExpression \s)
|         (singleExpression *)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression
|             (nonBinaryExpression
|               (compoundLiteral ()
|               (compoundLiteral gap)
|               (compoundLiteral
|                 (sequenceElements
|                   (singleExpression
|                     (nonBinaryExpression (literal (numericLiteral 100))))
|               (compoundLiteral gap)
|               (compoundLiteral ))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

You can multiply expressions and containers

### Division

```
$ echo  >test.qvr '1 / 1'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 1)))))
|         (singleExpression \s)
|         (singleExpression /)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 1)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

You can divide two number literals

```
$ echo  >test.qvr 'num / (1)'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container num))))
|         (singleExpression \s)
|         (singleExpression /)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression
|             (nonBinaryExpression
|               (compoundLiteral ()
|               (compoundLiteral gap)
|               (compoundLiteral
|                 (sequenceElements
|                   (singleExpression
|                     (nonBinaryExpression (literal (numericLiteral 1))))
|               (compoundLiteral gap)
|               (compoundLiteral ))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

You can divide expressions and containers

```
$ echo  >test.qvr '-5 / 3'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression -)
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 5))))
|         (singleExpression \s)
|         (singleExpression /)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 3)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

Here we are capturing the `-5` as a non-binary expression before adding the result to the main binary expression.

### Modulo

```
$ echo  >test.qvr '5 % 3'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 5)))))
|         (singleExpression \s)
|         (singleExpression %)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 3)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

### Power

```
$ echo  >test.qvr '2 ** 4'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 2)))))
|         (singleExpression \s)
|         (singleExpression **)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 4)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

### Logarithm

```
$ echo  >test.qvr '16 // 2'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 16))))
|         (singleExpression \s)
|         (singleExpression //)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 2)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

### Radication

```
$ echo  >test.qvr '4 /// 2'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (nonBinaryExpression (literal (numericLiteral 4)))))
|       (expressionStatement eos)
| (program \s)
| (program ///)
| (program \s)
| (program 2)
| (program \n)
| 
@ line 1:2 no viable alternative at input ' ///'
```

TODO: Fix radication operator error

## Precedence

```
$ echo  >test.qvr '1 + 3 * 2'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 1)))))
|         (singleExpression \s)
|         (singleExpression +)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression
|             (singleExpression (nonBinaryExpression (literal (numericLiteral 3))))
|           (singleExpression \s)
|           (singleExpression *)
|           (singleExpression \s)
|           (singleExpression
|             (singleExpression (nonBinaryExpression (literal (numericLiteral 2))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr '(1 + 3) * 2'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression
|             (nonBinaryExpression
|               (compoundLiteral ()
|               (compoundLiteral gap)
|               (compoundLiteral
|                 (sequenceElements
|                   (singleExpression
|                     (singleExpression
|                       (nonBinaryExpression (literal (numericLiteral 1))))
|                   (singleExpression \s)
|                   (singleExpression +)
|                   (singleExpression \s)
|                   (singleExpression
|                     (singleExpression
|                       (nonBinaryExpression (literal (numericLiteral 3))))
|               (compoundLiteral gap)
|               (compoundLiteral ))
|         (singleExpression \s)
|         (singleExpression *)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 2)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

