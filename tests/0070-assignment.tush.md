# Quarrel Grammar Test 0070

Here are the tests for assignment in Quarrel

## Assignment Expressions

### Reference Assignment
```
$ echo  >test.qvr 'container .= other'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container container))))
|         (singleExpression \s)
|         (singleExpression (assignmentOperator .=))
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container other))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

### Copy Assignment
```
$ echo  >test.qvr 'container := other'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container container))))
|         (singleExpression \s)
|         (singleExpression (assignmentOperator :=))
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container other))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```


