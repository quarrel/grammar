# Quarrel Grammar Test 0060

Here are the tests for basic conditionals in Quarrel

## Conditional Operators

```
$ echo  >test.qvr 'yes @@ 123'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (singleExpression (nonBinaryExpression (literal yes))))
|         (singleExpression (gap  ))
|         (singleExpression @@)
|         (singleExpression (gap  ))
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 123))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

The `@@` works like `ifTrue` in SmallTalk-like languages or a bit like `then` in many popular languages.

```
$ echo  >test.qvr 'yes ~@ 123'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (singleExpression (nonBinaryExpression (literal yes))))
|         (singleExpression (gap  ))
|         (singleExpression ~@)
|         (singleExpression (gap  ))
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 123))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

The `~@` works like `ifFalse` in SmallTalk-like languages.

```
$ echo  >test.qvr '() ?@ 123'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (singleExpression (nonBinaryExpression (literal ()))))
|         (singleExpression (gap  ))
|         (singleExpression ?@)
|         (singleExpression (gap  ))
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 123))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

Works like a maybe or elvis operator. Short-circuits and doesn't run right hand side if the left hand side is not null (Null in Quarrel is represented by an empty sequence `()`). Could be seen as `ifNull` but the difference from the other conditionals is that it would return the condition's result if not null.

## Combining Conditional Operators

```
$ echo  >test.qvr 'condition @@ trueResult ~@ falseResult'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression
|             (singleExpression (nonBinaryExpression (container condition))))
|           (singleExpression (gap  ))
|           (singleExpression @@)
|           (singleExpression (gap  ))
|           (singleExpression
|             (singleExpression (nonBinaryExpression (container trueResult))))
|         (singleExpression (gap  ))
|         (singleExpression ~@)
|         (singleExpression (gap  ))
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container falseResult))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'sys == "celsius" && temp << 37 @@'
$ echo >>test.qvr '    "Cold"'
$ echo >>test.qvr '~@ temp << 37 @@'
$ echo >>test.qvr '    "Not too cold"'
$ echo >>test.qvr '~@'
$ echo >>test.qvr '    "Normal"'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression
|             (singleExpression
|               (singleExpression
|                 (singleExpression
|                   (singleExpression (nonBinaryExpression (container sys))))
|                 (singleExpression \s)
|                 (singleExpression ==)
|                 (singleExpression \s)
|                 (singleExpression
|                   (singleExpression
|                     (singleExpression (nonBinaryExpression (literal "celsius")))
|                   (singleExpression \s)
|                   (singleExpression &&)
|                   (singleExpression \s)
|                   (singleExpression
|                     (singleExpression
|                       (singleExpression (nonBinaryExpression (container temp))))
|                     (singleExpression \s)
|                     (singleExpression <<)
|                     (singleExpression \s)
|                     (singleExpression
|                       (singleExpression
|                         (nonBinaryExpression (literal (numericLiteral 37))))
|               (singleExpression (gap  ))
|               (singleExpression @@)
|               (singleExpression (gap \n        ))
|               (singleExpression
|                 (singleExpression (nonBinaryExpression (literal "Cold"))))
|             (singleExpression (gap \n))
|             (singleExpression ~@)
|             (singleExpression (gap  ))
|             (singleExpression
|               (singleExpression
|                 (singleExpression (nonBinaryExpression (container temp))))
|               (singleExpression \s)
|               (singleExpression <<)
|               (singleExpression \s)
|               (singleExpression
|                 (singleExpression
|                   (nonBinaryExpression (literal (numericLiteral 37))))
|           (singleExpression (gap  ))
|           (singleExpression @@)
|           (singleExpression (gap \n        ))
|           (singleExpression
|             (singleExpression (nonBinaryExpression (literal "Not too cold"))))
|         (singleExpression (gap \n))
|         (singleExpression ~@)
|         (singleExpression (gap \n        ))
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal "Normal"))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```



