# Quarrel Grammar Test 0040

Here are the tests for basic literals in Quarrel

## Boolean literals

See 0003-basic_logic.tush.md

## Number literals

```
$ echo  >test.qvr '100'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (nonBinaryExpression (literal (numericLiteral 100)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr '0.1'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (nonBinaryExpression (literal (numericLiteral 0.1)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr '2^^1101'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (nonBinaryExpression (literal (numericLiteral 2^^1101))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

binary number

```
$ echo  >test.qvr '8^^2156307.1234'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (nonBinaryExpression (literal (numericLiteral 8^^2156307.1234))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

octal float

```
$ echo  >test.qvr '10.034^10'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (nonBinaryExpression (literal (numericLiteral 10.034^10))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

scientific notation

```
$ echo  >test.qvr '2.3^+2'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (nonBinaryExpression (literal (numericLiteral 2.3^+2))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

positive scientific

```
$ echo  >test.qvr '2.34243^-5'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (nonBinaryExpression (literal (numericLiteral 2.34243^-5))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

negative scientific

## Text literal

```
$ echo  >test.qvr '"foobar"'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (nonBinaryExpression (literal "foobar"))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr '"firstline`nsecondline"'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (nonBinaryExpression (literal "firstline`nsecondline")))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

