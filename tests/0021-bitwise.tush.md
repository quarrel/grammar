# Quarrel Grammar Test 0021

Here are the tests for bitwise operations in Quarrel

## Bitwise Expressions

### bitwise and
```
$ echo  >test.qvr '10 &&& 100'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 10))))
|         (singleExpression \s)
|         (singleExpression &&&)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 100))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

### bitwise xor
```
$ echo  >test.qvr 'container !!! 100'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container container))))
|         (singleExpression \s)
|         (singleExpression !!!)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 100))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

### bitwise or
```
$ echo  >test.qvr '10 ||| container'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 10))))
|         (singleExpression \s)
|         (singleExpression |||)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container container))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

### bitshift left
```
$ echo  >test.qvr '2^^1011 <<< 2'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression
|             (nonBinaryExpression (literal (numericLiteral 2^^1011))))
|         (singleExpression \s)
|         (singleExpression <<<)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 2)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'container <<< 2'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container container))))
|         (singleExpression \s)
|         (singleExpression <<<)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 2)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

### bitshift right
```
$ echo  >test.qvr '2^^1010 >>> container'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression
|             (nonBinaryExpression (literal (numericLiteral 2^^1010))))
|         (singleExpression \s)
|         (singleExpression >>>)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container container))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'container >>> container'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container container))))
|         (singleExpression \s)
|         (singleExpression >>>)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (container container))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```
