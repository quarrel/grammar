# Quarrel Grammar Test 0030

Here are the tests for basic math in Quarrel

## Boolean Literals

```
$ : # yes literal
$ echo  >test.qvr 'yes'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (nonBinaryExpression (literal yes))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ : # no literal
$ echo  >test.qvr 'no'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement (singleExpression (nonBinaryExpression (literal no))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ : # true literal
$ echo  >test.qvr 'true'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (nonBinaryExpression (literal true))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ : # false literal
$ echo >test.qvr 'false'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (nonBinaryExpression (literal false))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

## Logical Expressions

### Logical Operators

```
$ echo  >test.qvr 'true && yes'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal true))))
|         (singleExpression \s)
|         (singleExpression &&)
|         (singleExpression \s)
|         (singleExpression (singleExpression (nonBinaryExpression (literal yes))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'yes || no'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (singleExpression (nonBinaryExpression (literal yes))))
|         (singleExpression \s)
|         (singleExpression ||)
|         (singleExpression \s)
|         (singleExpression (singleExpression (nonBinaryExpression (literal no))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'true !! false'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal true))))
|         (singleExpression \s)
|         (singleExpression !!)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal false))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

### Identity Operators

```
$ echo  >test.qvr 'true == false'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal true))))
|         (singleExpression \s)
|         (singleExpression ==)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal false))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'yes <> no'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (singleExpression (nonBinaryExpression (literal yes))))
|         (singleExpression \s)
|         (singleExpression <>)
|         (singleExpression \s)
|         (singleExpression (singleExpression (nonBinaryExpression (literal no))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr 'yes === yes'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression (singleExpression (nonBinaryExpression (literal yes))))
|         (singleExpression \s)
|         (singleExpression ===)
|         (singleExpression \s)
|         (singleExpression (singleExpression (nonBinaryExpression (literal yes))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

### Comparison Operators

```
$ echo  >test.qvr '10 >> 5'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 10))))
|         (singleExpression \s)
|         (singleExpression >>)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 5)))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr '5 << 10'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 5)))))
|         (singleExpression \s)
|         (singleExpression <<)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 10))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr '10 <= 10'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 10))))
|         (singleExpression \s)
|         (singleExpression <=)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 10))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

```
$ echo  >test.qvr '15 >= 10'
$ cd .. && pp-s-exp ./tush-scratch/test.qvr
| (program
|   (sourceElements
|     (sourceElement
|       (expressionStatement
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 15))))
|         (singleExpression \s)
|         (singleExpression >=)
|         (singleExpression \s)
|         (singleExpression
|           (singleExpression (nonBinaryExpression (literal (numericLiteral 10))))
|       (expressionStatement (eos \n))
| (program <EOF>)
| 
```

