# Quarrel Grammar

Here lies the development workspace for the Quarrel grammar. Eventually this will be integrated into the main compiler project but presently, I wanted to test the ANTLR grammar to make sure it is comprehensive and complete before adding the actions that will evaluate the parse tree.

For all intents and purposes, this repo will be the **Quarrel specification**. I will tag releases at certain points when I start taking the grammar and integrating into the compiler but I will return to this repo when new syntax is being integrated.

## Development Environment

I've used Visual Studio Code and the excellent ANTLR [extension](https://github.com/mike-lischke/vscode-antlr4). Additionally, I have built into a couple VSCode tasks the use of the `grun` tool described in ANTLR documentation (a command-line usage of the TestRig class).

Additionally, I have used Intellij IDEA as they have an excellent ANTLR plugin that even provides labels in the parse tree. I'm not the most savvy with IDEA or a big IDE like this so I still default to VSCode for any development.

## Testing

I am going to use [tush](https://github.com/darius/tush) for my test framework. Each invocation will be to grun -tree and each result will be the sexpressions generated. This will allow rapid development and also function as a documentation place for the spec. As an added bonus, I find the symbol-based syntax of `tush` a perfect fit for Quarrel.