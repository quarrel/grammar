parser grammar QuarrelParser;

options {
    tokenVocab=QuarrelLexer;
    superClass=QuarrelParserBase;
}

program
    : HashBangLine? sourceElements? EOF
    ;

sourceElements
    : sourceElement+
    ;

sourceElement
    : expressionStatement
    | LineTerminator
    ;

expressionStatement
    : singleExpression eos
    ;

compoundLiteral
    : OpenParen gap sequenceElements gap CloseParen
    | OpenBracket gap listElements gap CloseBracket
    | OpenAngleBracket gap recordElements gap CloseAngleBracket
    ;

sequenceElements
    : singleExpression (Space* (Comma | LineTerminator) Space* singleExpression)*
    ;

listElements
    : listElement? (Space* (Comma | LineTerminator) Space* listElement)* Space* Comma*
    ;

listElement
    : Ellipsis? singleExpression
    | singleExpression Ellipsis?
    ;

recordElements
    : fieldAssignment (Space* (Comma | LineTerminator) Space* fieldAssignment)*
    ;

patternLiteral
    : OpenBrace patternElements CloseBrace
    ;

patternElements
    : Comma* Space* parameter? Space* (Comma Space* parameter)* Space* Comma*
    ;

parameter // TODO: add more parameter patterns
    : container
    | compoundLiteral
    | fieldAssignment
    ;

fieldOperator
    : Dot
    | Colon
    | QuestionMarkDot
    | QuestionMarkColon
    ;

fieldAssignment
    : fieldOperator fieldName                                                  # PriorContainerFieldAssignment
    | fieldName fieldOperator Space* singleExpression                                 # FieldExpressionAssignment
    | OpenAngleBracket Space* singleExpression Space* CloseAngleBracket fieldOperator Space* singleExpression # ComputedFieldExpressionAssignment
    | fieldName fieldOperator Space* formalRoutineExpression                          # RoutineField
    ;

fieldName
    : containerName
    | TextLiteral
    | numericLiteral
    | OpenBracket Space* singleExpression Space* CloseBracket
    ;



invocationExpression
    : invocable argumentElements+
    ;

invocable
    : compoundLiteral
    | container
    ;

argumentElements
    : Bang
    | OpenBracket Space* CloseBracket
    | OpenBracket Space* QuestionMark Space* CloseBracket
    | OpenBracket Space* arguments Space* CloseBracket
    | OpenAngleBracket Space* CloseBracket
    | OpenAngleBracket Space* (ColonUnderscore | UnderscoreColon) Space* CloseAngleBracket
    | OpenAngleBracket Space* arguments Space* CloseBracket
    | Space+ arguments
    ;

arguments
    : (singleExpression | fieldAssignment) Space* (Comma Space* (singleExpression | fieldAssignment))*
    ;

nonBinaryExpression
    : literal                                                                                 # LiteralExpression
    | specialArgument                                                                         # SpecArgExpression
    | invocationExpression                                                                    # InvokeExpression
    | container                                                                               # ContainerExpression
    | compoundLiteral                                                                         # CompoundExpression
    ;

singleExpression
    : nonBinaryExpression                                                                     # BaseExpression
    | singleExpression BSlash singleExpression                                                # MemberBSlashExpression
    | nonBinaryExpression DotDot                                                                 # UnaryPostfixExpression
    | singleExpression Space+ (Star | FSlash | Percent) Space+ singleExpression               # MultiplicativeExpression
    | singleExpression Space+ (Plus | Minus) Space+ singleExpression                          # AdditiveExpression
    | <assoc=right> (Plus | Minus | Tilde | Star | DotDot) nonBinaryExpression                # UnaryPrefixExpression
    | <assoc=right> singleExpression Space* (StarStar | FSlashFSlash) Space* singleExpression # ExponentiationExpression
    | singleExpression Space* (LtLtLt | GtGtGt) Space* singleExpression                       # BitShiftExpression
    | singleExpression Space* AmpAmpAmp Space* singleExpression                               # BitAndExpression
    | singleExpression Space* BangBangBang Space* singleExpression                            # BitXOrExpression
    | singleExpression Space* PipePipePipe Space* singleExpression                            # BitOrExpression
    | singleExpression Space* (LtLt | GtGt | LtEq | GtEq) Space* singleExpression             # RelationalExpression
    | singleExpression Space* AmpAmp Space* singleExpression                                  # LogicalAndExpression
    | singleExpression Space* BangBang Space* singleExpression                                # LogicalXorExpression
    | singleExpression Space* PipePipe Space* singleExpression                                # LogicalOrExpression
    | singleExpression Space* (EqEq | LtGt | EqEqEq ) Space* singleExpression                 # EqualityExpression
    | singleExpression Space* At Space* singleExpression                                      # InExpression
    | singleExpression gap (AtAt | TildeAt | QuestionMarkAt) gap singleExpression       # ThenExpression
    | singleExpression Space* rightTransform Space* singleExpression                          # TransformRightExpression
    | <assoc=right> singleExpression Space* leftTransform Space* singleExpression             # TransformLeftExpression
    | formalRoutineExpression                                                                 # RoutineExpression
    | <assoc=right> singleExpression Space* assignmentOperator Space* singleExpression        # AssignmentOperatorExpression
    | <assoc=right> singleExpression Space* BSlashEq gap recordElements                    # RecordAssignmentOperatorExpression
    ;

leftTransform
    : LtDash
    | LtDollar
    | DashLt
    | LtPipe
    | LtPipePipe
    | LtPipePipePipe
    ;

rightTransform
    : DashGt
    | DollarGt
    | GtDash
    | PipeGt
    | PipePipeGt
    | PipePipePipeGt
    ;

formalRoutineExpression
    : routinePattern Space+ (EqGt | StarGt | TildeGt) Space+ routineBody
    ;

routinePattern
    : patternLiteral
    | patternElements
    ;

routineBody
    : OpenParen gap sourceElements? gap CloseParen
    | singleExpression Space* (Comma Space* singleExpression)* gap
    | (eos | Space* LineTerminator )
    ;

assignmentOperator
    : DotEq
    | ColonEq
    | QuestionMarkEq
    | FSlashEq
    | CaretEq
    | PipeEq
    ;

literal
    : NullLiteral
    | BooleanLiteral
    | TextLiteral
    // | RegularExpressionLiteral
    | ioLiteral
    | numericLiteral
    ;

specialArgument
    : Underscore
    | QuestionMarkUnderscore
    ;

ioLiteral
    : PipeDashPipe
    | PipeEqPipe
    | FSlashEqFSlash
    ;

numericLiteral
    : DecimalLiteral
    | HexLiteral
    | OctalLiteral
    | BinaryLiteral
    ;

containerName
    : container
    | reservedWord
    ;

container
    : Space* BSlash? Container
    ;

reservedWord
    : BooleanLiteral
    ;

gap
    : (Space | LineTerminator)*
    ;

eos
    : Space* SemiColon
    | Space* EOF
    | Space* LineTerminator
    ;